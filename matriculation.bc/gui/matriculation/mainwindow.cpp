#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    Requester::requester().getSpecialities();

    ui->setupUi(this);

    ui->listsWidget->hide();
    ui->studentInfoWidget->hide();
    ui->scoreWidget->hide();
    ui->quotaWidget->hide();
    ui->orderWidget->hide();

    makeConnects();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::makeConnects()
{
    connect(ui->studentInfoWidget, &StudentInfoForm::onBackClicked, this, [this](){
        ui->studentInfoWidget->hide();
        ui->mainPageWidget->show();
    });

    connect(ui->scoreWidget, &ScoreForm::onBackClicked, this, [this](){
        ui->scoreWidget->hide();
        ui->mainPageWidget->show();
    });

    connect(ui->quotaWidget, &QuotaForm::onBackClicked, this, [this](){
        ui->quotaWidget->hide();
        ui->mainPageWidget->show();
    });

    connect(ui->listsWidget, &ListsForm::onBackClicked, this, [this](){
        ui->listsWidget->hide();
        ui->mainPageWidget->show();
    });

    connect(ui->orderWidget, &OrderForm::onBackClicked, this, [this](){
        ui->orderWidget->hide();
        ui->mainPageWidget->show();
    });

    connect(&Requester::requester(), &Requester::finishGetLists, this, [this](){
        ui->listsWidget->init();

        ui->mainPageWidget->hide();
        ui->listsWidget->show();
    });
    connect(&Requester::requester(), &Requester::finishGetOrderLists, this, [this](){
        ui->orderWidget->init();

        ui->mainPageWidget->hide();
        ui->orderWidget->show();
    });
}

void MainWindow::on_studentInfoBtn_clicked()
{
    ui->studentInfoWidget->clear();
    ui->studentInfoWidget->init();

    ui->mainPageWidget->hide();
    ui->studentInfoWidget->show();
}

void MainWindow::on_scoreBtn_clicked()
{
    ui->scoreWidget->clear();

    ui->mainPageWidget->hide();
    ui->scoreWidget->show();
}

void MainWindow::on_quotaBtn_clicked()
{
    ui->quotaWidget->clear();
    ui->quotaWidget->init();

    ui->mainPageWidget->hide();
    ui->quotaWidget->show();
}

void MainWindow::on_listsBtn_clicked()
{
    ui->listsWidget->clear();
    Requester::requester().getLists();
}

void MainWindow::on_orderBtn_clicked()
{
    ui->orderWidget->clear();
    Requester::requester().getListsForOrder();
}
