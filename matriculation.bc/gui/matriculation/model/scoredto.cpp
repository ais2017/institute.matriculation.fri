#include "scoredto.h"

ScoreDTO::ScoreDTO(int id_, QString name_, int score_)
{
    id = id_;
    name = name_;
    score = score_;
}

QJsonObject ScoreDTO::toJson() const
{
    QJsonObject obj;
    obj["_name"] = name;
    obj["id"] = id;
    obj["_score"] = score;
    return obj;
}


