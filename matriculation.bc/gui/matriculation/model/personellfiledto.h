#ifndef PERSONELLFILEDTO_H
#define PERSONELLFILEDTO_H

#include <QString>
#include <QJsonObject>
#include <QDebug>

class PersonellFileDTO
{
public:
    PersonellFileDTO(QJsonObject obj);
    PersonellFileDTO(QString fName = QString(), QString sName = QString(),
                     QString fll = QString(), QString docN = QString(),
                     QString edc = QString(), bool orig = false);

    QJsonObject toJson() const;

    QString getFullName() const;

    int getId() const;

    int getExamScore() const;

    bool getOriginal() const;

private:
    int id;
    QString fullName;
    QString specName;
    QString filial;
    QString documentNum;
    QString education;
    bool original;
    int examScore;
};

#endif // PERSONELLFILEDTO_H
