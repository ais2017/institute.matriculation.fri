#include "orderdto.h"

OrderDTO::OrderDTO()
{

}

void OrderDTO::setSpecialities(const QList<RatingSpecialityDTO> &value)
{
    specialities = value;
}

QDate OrderDTO::getDate() const
{
    return date;
}

void OrderDTO::setDate(const QDate &value)
{
    date = value;
}

QString OrderDTO::getText() const
{
    return text;
}

void OrderDTO::setText(const QString &value)
{
    text = value;
}

void OrderDTO::addSpeciality(const RatingSpecialityDTO &speciality)
{
    specialities.append(speciality);
}

QList<RatingSpecialityDTO> OrderDTO::getSpecialities() const
{
    return specialities;
}
