#ifndef SPECIALITYDTO_H
#define SPECIALITYDTO_H

#include <QString>
#include <QJsonObject>

class SpecialityDTO
{
public:
    SpecialityDTO(QJsonObject obj);
    SpecialityDTO(QString name_ = QString(), int places_quantity_ = 0);

    QString getName() const;

    int getPlaces_quantity() const;

    QJsonObject toJson() const;

private:
    QString name;
    int placesQuantity;
};

#endif // SPECIALITYDTO_H
