#include "specialitydto.h"
#include <QJsonDocument>


SpecialityDTO::SpecialityDTO(QJsonObject obj)
{
    name = obj["_name"].toString();
    placesQuantity = obj["_quota"].toInt();
}

SpecialityDTO::SpecialityDTO(QString name_, int places_quantity_)
{
    name = name_;
    placesQuantity = places_quantity_;
}

QString SpecialityDTO::getName() const
{
    return name;
}

int SpecialityDTO::getPlaces_quantity() const
{
    return placesQuantity;
}

QJsonObject SpecialityDTO::toJson() const
{
    QJsonObject obj;
    obj["_name"] = name;
    obj["_quota"] = placesQuantity;
    return obj;
}
