#include "personellfiledto.h"

PersonellFileDTO::PersonellFileDTO(QJsonObject obj)
{
    id = obj["_number"].toInt();
    fullName = obj["_applicant"].toString();
    original = obj["_original"].toBool();
    examScore = obj["_exam_score"].toInt();
}

PersonellFileDTO::PersonellFileDTO(QString fName, QString sName, QString fll,
                                   QString docN, QString edc, bool orig)
{
    id = -1;
    fullName = fName;
    specName = sName;
    filial = fll;
    documentNum = docN;
    education = edc;
    original = orig;
}

QJsonObject PersonellFileDTO::toJson() const
{
    QJsonObject obj;
    obj["_applicant"] = fullName;
    obj["_course"] = specName;
    obj["_branch"] = filial;
    obj["_document"] = documentNum;
    obj["_education"] = education;
    obj["_original"] = original;
    return obj;
}

QString PersonellFileDTO::getFullName() const
{
    return fullName;
}

int PersonellFileDTO::getId() const
{
    return id;
}

int PersonellFileDTO::getExamScore() const
{
    return examScore;
}

bool PersonellFileDTO::getOriginal() const
{
    return original;
}
