#include "ratingspecialitydto.h"

RatingSpecialityDTO::RatingSpecialityDTO()
{
    name = QString();
    placesQuantity = 0;
}

RatingSpecialityDTO::RatingSpecialityDTO(QJsonObject obj)
{
    name = obj["_name"].toString();
    placesQuantity = obj["_quota"].toInt();
    auto rating = obj["_rating"].toObject();
    auto filesArray = rating["_files"].toObject();
    for (auto pfObj: filesArray) {
        PersonellFileDTO pfNew = PersonellFileDTO(pfObj.toObject());
        idToFile.insert(pfNew.getId(), pfNew);
    }
}

QString RatingSpecialityDTO::getName() const
{
    return name;
}

int RatingSpecialityDTO::getPlacesQuantity() const
{
    return placesQuantity;
}

QMap<int, PersonellFileDTO> RatingSpecialityDTO::getIdToFile() const
{
    return idToFile;
}
