#ifndef RATINGSPECIALITYDTO_H
#define RATINGSPECIALITYDTO_H

#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QMap>
#include "personellfiledto.h"

class RatingSpecialityDTO
{
public:
    RatingSpecialityDTO();
    RatingSpecialityDTO(QJsonObject obj);

    QString getName() const;

    int getPlacesQuantity() const;

    QMap<int, PersonellFileDTO> getIdToFile() const;

private:
    QString name;
    int placesQuantity;

    QMap<int, PersonellFileDTO> idToFile;
};

#endif // RATINGSPECIALITYDTO_H
