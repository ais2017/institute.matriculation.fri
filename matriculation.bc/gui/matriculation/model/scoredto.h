#ifndef SCOREDTO_H
#define SCOREDTO_H

#include <QString>
#include <QJsonObject>

class ScoreDTO
{
public:
    ScoreDTO(int id_ = 0, QString name_ = QString(), int score_ = 0);

    QJsonObject toJson() const;

private:
    int id;
    QString name;
    int score;
};

#endif // SCOREDTO_H
