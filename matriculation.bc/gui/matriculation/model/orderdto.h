#ifndef ORDERDTO_H
#define ORDERDTO_H

#include <QDate>
#include <QList>
#include "model/ratingspecialitydto.h"

class OrderDTO
{
public:
    OrderDTO();

    QList<RatingSpecialityDTO> getSpecialities() const;
    void setSpecialities(const QList<RatingSpecialityDTO> &value);

    QDate getDate() const;
    void setDate(const QDate &value);

    QString getText() const;
    void setText(const QString &value);

    void addSpeciality(const RatingSpecialityDTO &speciality);

private:
    QDate date;
    QString text;
    QList<RatingSpecialityDTO> specialities;
};

#endif // ORDERDTO_H
