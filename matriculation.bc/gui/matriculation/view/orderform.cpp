#include "orderform.h"
#include "ui_orderform.h"

OrderForm::OrderForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OrderForm)
{
    ui->setupUi(this);
}

OrderForm::~OrderForm()
{
    delete ui;
}

void OrderForm::init()
{
    auto ratingLists = SpecialitiesController::controller().getOrderLists();
    for (auto rating: ratingLists) {
        SpecialityRatingForm *specRating = new SpecialityRatingForm();
        specRating->setShowCheckBox(true);
        specRating->init(rating);
        ui->listLayout->addWidget(specRating);
        specRating->adjustSize();
        widgetsInList.append(specRating);
    }
}

void OrderForm::clear()
{
    for (auto w: widgetsInList) {
        ui->listLayout->removeWidget(w);
    }
    widgetsInList.clear();

    ui->plainTextEdit->clear();
}

void OrderForm::on_backBtn_clicked()
{
    emit onBackClicked();
}

void OrderForm::on_saveBtn_clicked()
{
    OrderDTO orderDTO;
    orderDTO.setDate(QDate::currentDate());
    orderDTO.setText(ui->plainTextEdit->toPlainText());
    for (auto w : widgetsInList) {
        SpecialityRatingForm *specRating =
                dynamic_cast<SpecialityRatingForm *>(w);
        if (specRating->isChecked()) {
            orderDTO.addSpeciality(specRating->getInfo());
        }
    }

    OrderBuilder *builder = new OrderBuilder();
    connect(builder, &OrderBuilder::finishBuildOrder,
            this, &OrderForm::on_backBtn_clicked);
    builder->buildOrder(orderDTO);
}
