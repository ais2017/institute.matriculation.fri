#ifndef SCOREFORM_H
#define SCOREFORM_H

#include <QWidget>
#include "model/scoredto.h"
#include "controller/requester.h"

namespace Ui {
class ScoreForm;
}

class ScoreForm : public QWidget
{
    Q_OBJECT

public:
    explicit ScoreForm(QWidget *parent = 0);
    ~ScoreForm();

    void clear();

signals:
    void onBackClicked();

private slots:
    void on_backBtn_clicked();

    void on_saveBtn_clicked();

private:
    Ui::ScoreForm *ui;

    bool checkInput();

    void makeConnects();
};

#endif // SCOREFORM_H
