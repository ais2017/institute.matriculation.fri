#include "specialityratingform.h"
#include "ui_specialityratingform.h"

SpecialityRatingForm::SpecialityRatingForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpecialityRatingForm)
{
    ui->setupUi(this);

    ui->tableWidget->horizontalHeader()->
            setSectionResizeMode(Columns::ID, QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->
            setSectionResizeMode(Columns::FIO, QHeaderView::Stretch);
    ui->tableWidget->horizontalHeader()->
            setSectionResizeMode(Columns::SCORE, QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->
            setSectionResizeMode(Columns::ORIGINAL, QHeaderView::ResizeToContents);

    info = RatingSpecialityDTO();
}

SpecialityRatingForm::~SpecialityRatingForm()
{
    delete ui;
}

void SpecialityRatingForm::init(const RatingSpecialityDTO &rating)
{
    info = rating;

    ui->nameLabel->setText(rating.getName());

    auto files = rating.getIdToFile();
    for (auto pf: files) {
        int row = ui->tableWidget->rowCount();
        ui->tableWidget->insertRow(row);
        ui->tableWidget->setItem(row, int(Columns::ID),
                                 new IntTableWidgetItem(QString::number(pf.getId())));
        ui->tableWidget->setItem(row, int(Columns::FIO),
                                 new QTableWidgetItem(pf.getFullName()));
        ui->tableWidget->setItem(row, int(Columns::SCORE),
                                 new IntTableWidgetItem(QString::number(pf.getExamScore())));
        QString originalLine;
        if (pf.getOriginal()) {
            originalLine = "+";
        } else {
            originalLine = "-";
        }
        ui->tableWidget->setItem(row, int(Columns::ORIGINAL),
                                 new IntTableWidgetItem(originalLine));


        ui->tableWidget->item(row, Columns::ID)->setTextAlignment(Qt::AlignHCenter);
        ui->tableWidget->item(row, Columns::FIO)->setTextAlignment(Qt::AlignHCenter);
        ui->tableWidget->item(row, Columns::SCORE)->setTextAlignment(Qt::AlignHCenter);
        ui->tableWidget->item(row, Columns::ORIGINAL)->setTextAlignment(Qt::AlignHCenter);
    }

    ui->tableWidget->sortByColumn(Columns::SCORE, Qt::SortOrder::DescendingOrder);

    int passedCount = ui->tableWidget->rowCount();
    if (passedCount >= rating.getPlacesQuantity()) {
        passedCount = rating.getPlacesQuantity();
    }

    for (int i = 0; i < passedCount; i++) {
        QColor passColor = QColor("#1f7723");
        ui->tableWidget->item(i, Columns::ID)->setTextColor(passColor);
        ui->tableWidget->item(i, Columns::FIO)->setTextColor(passColor);
        ui->tableWidget->item(i, Columns::SCORE)->setTextColor(passColor);
        ui->tableWidget->item(i, Columns::ORIGINAL)->setTextColor(passColor);
    }

    ui->tableWidget->adjustSize();
}

void SpecialityRatingForm::clear()
{
    ui->tableWidget->clear();
}

void SpecialityRatingForm::setShowCheckBox(bool value)
{
    ui->checkBox->setVisible(value);
}

bool SpecialityRatingForm::isChecked()
{
    return ui->checkBox->isChecked();
}

RatingSpecialityDTO SpecialityRatingForm::getInfo() const
{
    return info;
}
