#include "studentinfoform.h"
#include "ui_studentinfoform.h"

StudentInfoForm::StudentInfoForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StudentInfoForm)
{
    ui->setupUi(this);

    makeConnects();
}

StudentInfoForm::~StudentInfoForm()
{
    delete ui;
}

void StudentInfoForm::init()
{
    ui->specComboBox->clear();
    auto specNames = SpecialitiesController::controller().getNames();
    for (QString name: specNames) {
        ui->specComboBox->addItem(name);
    }
}

void StudentInfoForm::clear()
{
    ui->lineEditDocNum->clear();
    ui->lineEditFilial->clear();
    ui->lineEditFname->clear();
    ui->lineEditEducation->clear();
    ui->lineEditEducation->clear();
    ui->checkBox->setChecked(false);
    ui->errorLabel->setVisible(false);
    ui->saveBtn->setDisabled(false);
}

void StudentInfoForm::on_backBtn_clicked()
{
    emit onBackClicked();
}

void StudentInfoForm::on_saveBtn_clicked()
{
    ui->errorLabel->setVisible(false);
    ui->saveBtn->setDisabled(true);
    if (!checkInput()) {
        ui->saveBtn->setDisabled(false);
        ui->errorLabel->setVisible(true);
        return;
    }
    PersonellFileDTO pf(ui->lineEditFname->text(), ui->specComboBox->currentText(),
                        ui->lineEditFilial->text(), ui->lineEditDocNum->text(),
                        ui->lineEditEducation->text(), ui->checkBox->isChecked());

    Requester::requester().addPersonellFile(pf.toJson());
}

void StudentInfoForm::makeConnects()
{
    connect(&Requester::requester(), &Requester::finishAddPersonellFile,
            this, [this](bool result){
        ui->saveBtn->setDisabled(false);
        ui->errorLabel->setVisible(!result);
        if (result) {
            on_backBtn_clicked();
        }
    });
}

bool StudentInfoForm::checkInput()
{
    return !ui->lineEditFname->text().isEmpty() &&
            !ui->lineEditDocNum->text().isEmpty() &&
            !ui->lineEditEducation->text().isEmpty() &&
            !ui->lineEditFilial->text().isEmpty();
}
