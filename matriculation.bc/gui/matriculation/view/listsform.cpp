#include "listsform.h"
#include "ui_listsform.h"

ListsForm::ListsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ListsForm)
{
    ui->setupUi(this);
}

ListsForm::~ListsForm()
{
    delete ui;
}

void ListsForm::init()
{
    auto ratingLists = SpecialitiesController::controller().getRatingLists();
    for (auto rating: ratingLists) {
        SpecialityRatingForm *specRating = new SpecialityRatingForm();
        specRating->setShowCheckBox(false);
        specRating->init(rating);
        ui->listLayout->addWidget(specRating);
        specRating->adjustSize();
        widgetsInList.append(specRating);
    }
}

void ListsForm::clear()
{
    for (auto w: widgetsInList) {
        ui->listLayout->removeWidget(w);
    }
    widgetsInList.clear();
}

void ListsForm::on_backBtn_clicked()
{
    emit onBackClicked();
}
