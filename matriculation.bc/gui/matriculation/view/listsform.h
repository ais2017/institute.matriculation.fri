#ifndef LISTSFORM_H
#define LISTSFORM_H

#include <QWidget>
#include <QVBoxLayout>
#include "specialityratingform.h"
#include "controller/specialitiescontroller.h"

namespace Ui {
class ListsForm;
}

class ListsForm : public QWidget
{
    Q_OBJECT

public:
    explicit ListsForm(QWidget *parent = 0);
    ~ListsForm();

    void init();

    void clear();

signals:
    void onBackClicked();

private slots:
    void on_backBtn_clicked();

private:
    Ui::ListsForm *ui;

    QList<QWidget *> widgetsInList;
};

#endif // LISTSFORM_H
