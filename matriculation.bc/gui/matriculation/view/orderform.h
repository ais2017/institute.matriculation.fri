#ifndef ORDERFORM_H
#define ORDERFORM_H

#include <QWidget>
#include "specialityratingform.h"
#include "controller/specialitiescontroller.h"
#include "model/orderdto.h"
#include "utils/orderbuilder.h"

namespace Ui {
class OrderForm;
}

class OrderForm : public QWidget
{
    Q_OBJECT

public:
    explicit OrderForm(QWidget *parent = 0);
    ~OrderForm();

    void init();

    void clear();

signals:
    void onBackClicked();

private slots:
    void on_backBtn_clicked();

    void on_saveBtn_clicked();

private:
    Ui::OrderForm *ui;

    QList<QWidget *> widgetsInList;
};

#endif // ORDERFORM_H
