#ifndef STUDENTINFOFORM_H
#define STUDENTINFOFORM_H

#include <QWidget>
#include "controller/specialitiescontroller.h"
#include "controller/requester.h"
#include "model/personellfiledto.h"

namespace Ui {
class StudentInfoForm;
}

class StudentInfoForm : public QWidget
{
    Q_OBJECT

public:
    explicit StudentInfoForm(QWidget *parent = 0);
    ~StudentInfoForm();

    void init();

    void clear();

signals:
    void onBackClicked();

private slots:
    void on_backBtn_clicked();

    void on_saveBtn_clicked();

private:
    Ui::StudentInfoForm *ui;

    void makeConnects();

    bool checkInput();
};

#endif // STUDENTINFOFORM_H
