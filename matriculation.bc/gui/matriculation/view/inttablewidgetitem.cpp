#include "inttablewidgetitem.h"

IntTableWidgetItem::IntTableWidgetItem(const QString &text) : QTableWidgetItem(text)
{

}

bool IntTableWidgetItem::operator <(const QTableWidgetItem &other) const
{
    return text().toInt() < other.text().toInt();
}

