#ifndef SPECIALITYRATINGFORM_H
#define SPECIALITYRATINGFORM_H

#include <QWidget>
#include "model/ratingspecialitydto.h"
#include <QHeaderView>
#include "view/inttablewidgetitem.h"

namespace Ui {
class SpecialityRatingForm;
}

class SpecialityRatingForm : public QWidget
{
    Q_OBJECT

public:
    explicit SpecialityRatingForm(QWidget *parent = 0);
    ~SpecialityRatingForm();

    void init(const RatingSpecialityDTO &rating);

    void clear();

    void setShowCheckBox(bool value);
    bool isChecked();

    RatingSpecialityDTO getInfo() const;

private:
    Ui::SpecialityRatingForm *ui;

    RatingSpecialityDTO info;

    enum Columns {ID, FIO, SCORE, ORIGINAL};
};

#endif // SPECIALITYRATINGFORM_H
