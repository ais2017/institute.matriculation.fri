#include "quotaform.h"
#include "ui_quotaform.h"

QuotaForm::QuotaForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QuotaForm)
{
    ui->setupUi(this);

    makeConnects();
}

QuotaForm::~QuotaForm()
{
    delete ui;
}

void QuotaForm::init()
{
    ui->comboBoxSpec->clear();
    auto specNames = SpecialitiesController::controller().getNames();
    for (QString name: specNames) {
        ui->comboBoxSpec->addItem(name);
    }
}

void QuotaForm::clear()
{
    ui->lineEditQuota->clear();
    ui->errorLabel->setVisible(false);
    ui->saveBtn->setDisabled(false);
}

void QuotaForm::on_backBtn_clicked()
{
    emit onBackClicked();
}

void QuotaForm::on_saveBtn_clicked()
{
    ui->errorLabel->setVisible(false);
    ui->saveBtn->setDisabled(true);
    if (!checkInput()) {
        ui->saveBtn->setDisabled(false);
        ui->errorLabel->setVisible(true);
        return;
    }
    SpecialityDTO sp(ui->comboBoxSpec->currentText(),
                     ui->lineEditQuota->text().toInt());
    Requester::requester().setQuota(sp.toJson());
}

bool QuotaForm::checkInput()
{
    bool quotaInt;
    int quota = ui->lineEditQuota->text().toInt(&quotaInt);

    return !ui->lineEditQuota->text().isEmpty() &&
            quotaInt && quota > 0;
}

void QuotaForm::makeConnects()
{
    connect(&Requester::requester(), &Requester::finishSetQuota,
            this, [this](bool result){
        ui->saveBtn->setDisabled(false);
        ui->errorLabel->setVisible(!result);
        if (result) {
            on_backBtn_clicked();
        }
    });
}
