#ifndef INTTABLEWIDGETITEM_H
#define INTTABLEWIDGETITEM_H

#include <QTableWidgetItem>


class IntTableWidgetItem : public QTableWidgetItem
{
public:
    IntTableWidgetItem(const QString &text);
    bool operator <(const QTableWidgetItem &other) const;
};

#endif // INTTABLEWIDGETITEM_H
