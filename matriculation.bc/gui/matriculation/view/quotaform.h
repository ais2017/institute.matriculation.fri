#ifndef QUOTAFORM_H
#define QUOTAFORM_H

#include <QWidget>
#include "controller/requester.h"
#include "controller/specialitiescontroller.h"

namespace Ui {
class QuotaForm;
}

class QuotaForm : public QWidget
{
    Q_OBJECT

public:
    explicit QuotaForm(QWidget *parent = 0);
    ~QuotaForm();

    void init();

    void clear();

signals:
    void onBackClicked();

private slots:
    void on_backBtn_clicked();

    void on_saveBtn_clicked();

private:
    Ui::QuotaForm *ui;

    bool checkInput();

    void makeConnects();
};

#endif // QUOTAFORM_H
