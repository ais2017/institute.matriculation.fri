#include "scoreform.h"
#include "ui_scoreform.h"

ScoreForm::ScoreForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScoreForm)
{
    ui->setupUi(this);
    ui->errorLabel->hide();

    makeConnects();
}

ScoreForm::~ScoreForm()
{
    delete ui;
}

void ScoreForm::clear()
{
    ui->nameLineEdit->clear();
    ui->scoreLineEdit->clear();
    ui->numLineEdit->clear();

    ui->errorLabel->setVisible(false);
}

void ScoreForm::on_backBtn_clicked()
{
    emit onBackClicked();
}

void ScoreForm::on_saveBtn_clicked()
{
    ui->errorLabel->setVisible(false);
    ui->saveBtn->setDisabled(true);
    if (!checkInput()) {
        ui->saveBtn->setDisabled(false);
        ui->errorLabel->setVisible(true);
        return;
    }
    ScoreDTO sc(ui->numLineEdit->text().toInt(),
                ui->nameLineEdit->text(), ui->scoreLineEdit->text().toInt());
    Requester::requester().addExamScore(sc.toJson());
}

bool ScoreForm::checkInput()
{
    bool numInt;
    int value = ui->numLineEdit->text().toInt(&numInt);
    bool scoreInt;
    int score = ui->scoreLineEdit->text().toInt(&scoreInt);

    return !ui->numLineEdit->text().isEmpty() && numInt && scoreInt &&
            0 <= score && score <= 100 &&
            !ui->nameLineEdit->text().isEmpty() &&
            !ui->scoreLineEdit->text().isEmpty();
}

void ScoreForm::makeConnects()
{
    connect(&Requester::requester(), &Requester::finishAddScore,
            this, [this](bool result){
        ui->saveBtn->setDisabled(false);
        ui->errorLabel->setVisible(!result);
        if (result) {
            on_backBtn_clicked();
        }
    });
}
