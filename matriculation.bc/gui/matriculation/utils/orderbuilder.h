#ifndef ORDERBUILDER_H
#define ORDERBUILDER_H

#include <QObject>
#include <QDate>
#include <QFile>
#include <QTextStream>
#include <QTextEdit>
#include "model/orderdto.h"
#include <QStandardPaths>
#include <QPrinter>
#include <QDir>

class OrderBuilder : public QObject
{
    Q_OBJECT
public:
    OrderBuilder(QObject *parent = 0);

    void buildOrder(const OrderDTO &orderDTO);

    void clearOrder();

signals:
    void finishBuildOrder();

private:
    QString orderTemplate;
    QTextEdit *editor;

    QString ordersPath =
            QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) +
            QString("/orders/");
};

#endif // ORDERBUILDER_H
