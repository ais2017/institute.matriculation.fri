#include "orderbuilder.h"

OrderBuilder::OrderBuilder(QObject *parent) : QObject(parent)
{
    editor = new QTextEdit();
    clearOrder();

    if (!QDir(ordersPath).exists()) {
        QDir().mkdir(ordersPath);
    }
}

void OrderBuilder::buildOrder(const OrderDTO &orderDTO)
{
    orderTemplate = orderTemplate
            .arg(orderDTO.getDate().toString("dd-MM-yyyy"))
            .arg(orderDTO.getText());
    orderTemplate.replace("%3", "%1");

    QString specialitiesListStr;
    auto specialitiesDTOs = orderDTO.getSpecialities();
    for (auto spec : specialitiesDTOs) {
        specialitiesListStr += "<h4 align = \"left\"> " + spec.getName() + " </h3>";
        auto ratingList = spec.getIdToFile();

        QString ratingListStr;
        ratingListStr += " <table border=\"1\" width=\"100%\" cellpadding=\"5\">";
        for (auto pf : ratingList) {
            ratingListStr += QString("<td>%1</td>").arg(pf.getFullName());
        }
        ratingListStr += "</table> <br> <br>";

        specialitiesListStr += ratingListStr;
    }

    orderTemplate = orderTemplate.arg(specialitiesListStr);

    editor->append(orderTemplate);

    QString orderName = ordersPath +
            QString("order_%1").arg(orderDTO.getDate().toString("dd-MM-yyyy"));
    auto doc = editor->document();
    QPrinter *printer = new QPrinter(QPrinter::HighResolution);
    printer->setOutputFormat(QPrinter::PdfFormat);
    printer->setOutputFileName(orderName + ".pdf");
    doc->print(printer);

    emit finishBuildOrder();
}

void OrderBuilder::clearOrder()
{
    QFile tempfile("/home/avsimonenko/Documents/study/MAGA/1sem/AIS/repos/institute.matriculation.fri/matriculation.bc/gui/matriculation/res/order_template.html");
    QTextStream temp(&tempfile);
    temp.setCodec("UTF-8");
    tempfile.open(QFile::ReadOnly|QFile::Text);
    orderTemplate = QString(temp.readAll());
    tempfile.close();
}
