#include "requester.h"
#include "model/specialitydto.h"

Requester *Requester::instance = nullptr;

Requester::Requester(QObject *parent) : QObject(parent)
{
    manager = new QNetworkAccessManager(this);

    connect(manager, &QNetworkAccessManager::finished,
            this, &Requester::requestFinished);
}

void Requester::requestFinished(QNetworkReply *reply)
{
    QVariant possibleRedirectUrl =
                 reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    QUrl _urlRedirectedTo = this->redirectUrl(possibleRedirectUrl.toUrl(),
                                             _urlRedirectedTo);
    if(!_urlRedirectedTo.isEmpty()) {
            manager->get(QNetworkRequest(_urlRedirectedTo));
    }
    else {
        if (reply->url().toString().contains("specialities")) {
            QString answer = reply->readAll();
            qDebug() << "specialities answer: " << answer;
            SpecialitiesController::controller().handleSpecialitiesRequest(answer);
            return;
        }
        if (reply->url().toString().contains("add_personell_file")) {
            QString answer = reply->readAll();
            qDebug() << "add_personell_file answer: " << answer;
            QJsonDocument doc = QJsonDocument::fromJson(answer.toUtf8());
            QJsonObject responce = doc.object();
            emit finishAddPersonellFile(responce["result"].toString() == "OK");
            return;
        }
        if (reply->url().toString().contains("add_score")) {
            QString answer = reply->readAll();
            qDebug() << "add_score answer: " << answer;
            QJsonDocument doc = QJsonDocument::fromJson(answer.toUtf8());
            QJsonObject responce = doc.object();
            emit finishAddScore(responce["result"].toString() == "OK");
            return;
        }
        if (reply->url().toString().contains("get_lists_for_order")) {
            QString answer = reply->readAll();
            qDebug() << "get_lists_for_order answer: " << answer;
            SpecialitiesController::controller().handleOrderListsRequest(answer);
            emit finishGetOrderLists();
            return;
        }
        if (reply->url().toString().contains("get_lists")) {
            QString answer = reply->readAll();
            qDebug() << "get_lists answer: " << answer;
            SpecialitiesController::controller().handleListsRequest(answer);
            emit finishGetLists();
            return;
        }
        if (reply->url().toString().contains("set_quota")) {
            QString answer = reply->readAll();
            qDebug() << "set_quota answer: " << answer;
            QJsonDocument doc = QJsonDocument::fromJson(answer.toUtf8());
            QJsonObject responce = doc.object();
            emit finishSetQuota(responce["result"].toString() == "OK");
            return;
        }
    }
}

Requester &Requester::requester()
{
    if (instance == nullptr) {
        instance = new Requester();
    }
    return *instance;
}

void Requester::getSpecialities()
{
    manager->get(QNetworkRequest(QUrl(serverURL + "specialities")));
}

void Requester::addPersonellFile(const QJsonObject &obj)
{
    QNetworkRequest request(QUrl(serverURL + "add_personell_file"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    manager->post(request, QJsonDocument(obj).toJson());
}

void Requester::addExamScore(const QJsonObject &obj)
{
    QNetworkRequest request(QUrl(serverURL + "add_score"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    manager->post(request, QJsonDocument(obj).toJson());
}

void Requester::setQuota(const QJsonObject &obj)
{
    QNetworkRequest request(QUrl(serverURL + "set_quota"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    manager->post(request, QJsonDocument(obj).toJson());
}

void Requester::getLists()
{
    manager->get(QNetworkRequest(QUrl(serverURL + "get_lists")));
}

void Requester::getListsForOrder()
{
    manager->get(QNetworkRequest(QUrl(serverURL + "get_lists_for_order")));
}

QUrl Requester::redirectUrl(const QUrl& possibleRedirectUrl,
                               const QUrl& oldRedirectUrl) const {
    QUrl redirectUrl;
    if(!possibleRedirectUrl.isEmpty() &&
       possibleRedirectUrl != oldRedirectUrl) {
        redirectUrl = possibleRedirectUrl;
    }
    return redirectUrl;
}
