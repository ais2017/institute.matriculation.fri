#ifndef SPECIALITIESCONTROLLER_H
#define SPECIALITIESCONTROLLER_H

#include <QMap>
#include <QString>
#include "model/specialitydto.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
#include "model/ratingspecialitydto.h"

class SpecialitiesController
{
public:
    static SpecialitiesController &controller();

    void handleSpecialitiesRequest(const QString &answer);
    void handleListsRequest(const QString &answer);
    void handleOrderListsRequest(const QString &answer);

    QList<QString> getNames() const;

    QList<RatingSpecialityDTO> getRatingLists() const;

    QList<RatingSpecialityDTO> getOrderLists() const;

private:
    SpecialitiesController();
    static SpecialitiesController *instance;

    QMap<QString, SpecialityDTO> nameToSpeciality;
    QList<RatingSpecialityDTO> ratingLists;
    QList<RatingSpecialityDTO> orderLists;
};

#endif // SPECIALITIESCONTROLLER_H
