#include "specialitiescontroller.h"

SpecialitiesController *SpecialitiesController::instance = nullptr;

SpecialitiesController::SpecialitiesController()
{

}

QList<RatingSpecialityDTO> SpecialitiesController::getOrderLists() const
{
    return orderLists;
}

QList<RatingSpecialityDTO> SpecialitiesController::getRatingLists() const
{
    return ratingLists;
}

SpecialitiesController &SpecialitiesController::controller()
{
    if (instance == nullptr) {
        instance = new SpecialitiesController();
    }
    return *instance;
}

void SpecialitiesController::handleSpecialitiesRequest(const QString &answer)
{
    nameToSpeciality.clear();
    QString toParse = answer;
    toParse.replace('\'', '"');
    toParse.replace("False", "false");
    toParse.replace("True", "true");

    QJsonDocument doc = QJsonDocument::fromJson(toParse.toUtf8());
    QJsonArray array = doc.array();
    for (auto obj: array) {
        SpecialityDTO specDTO(obj.toObject());
        nameToSpeciality.insert(specDTO.getName(), specDTO);
    }
}

void SpecialitiesController::handleListsRequest(const QString &answer)
{
    ratingLists.clear();

    QString toParse = answer;
    toParse.replace('\'', '"');
    toParse.replace("False", "false");
    toParse.replace("True", "true");

    QJsonDocument doc = QJsonDocument::fromJson(toParse.toUtf8());
    QJsonArray array = doc.array();
    for (auto obj: array) {
        RatingSpecialityDTO specDTO(obj.toObject());
        ratingLists.append(specDTO);
    }
}

void SpecialitiesController::handleOrderListsRequest(const QString &answer)
{
    orderLists.clear();

    QString toParse = answer;
    toParse.replace('\'', '"');
    toParse.replace("False", "false");
    toParse.replace("True", "true");

    QJsonDocument doc = QJsonDocument::fromJson(toParse.toUtf8());
    QJsonArray array = doc.array();
    for (auto obj: array) {
        RatingSpecialityDTO specDTO(obj.toObject());
        orderLists.append(specDTO);
    }
}

QList<QString> SpecialitiesController::getNames() const
{
    return nameToSpeciality.keys();
}
