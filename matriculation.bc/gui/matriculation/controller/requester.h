#ifndef REQUESTER_H
#define REQUESTER_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QJsonArray>
#include <QJsonDocument>

#include "specialitiescontroller.h"

class Requester : public QObject
{
    Q_OBJECT
public:
    static Requester &requester();

    void getSpecialities();

    void addPersonellFile(const QJsonObject &obj);

    void addExamScore(const QJsonObject &obj);

    void setQuota(const QJsonObject &obj);

    void getLists();

    void getListsForOrder();

signals:
    void finishAddPersonellFile(bool res);
    void finishAddScore(bool res);
    void finishGetLists();
    void finishGetOrderLists();
    void finishSetQuota(bool res);

private:
    Requester(QObject *parent = 0);
    static Requester *instance;

    QNetworkAccessManager *manager;

    QString serverURL = "http://127.0.0.1:8000/";

    void requestFinished(QNetworkReply *reply);

    QUrl redirectUrl(const QUrl& possibleRedirectUrl,
                                   const QUrl& oldRedirectUrl) const;
};

#endif // REQUESTER_H
