#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "view/studentinfoform.h"
#include "view/scoreform.h"
#include "view/quotaform.h"
#include "view/listsform.h"
#include "view/orderform.h"
#include "controller/requester.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_studentInfoBtn_clicked();

    void on_scoreBtn_clicked();

    void on_quotaBtn_clicked();

    void on_listsBtn_clicked();

    void on_orderBtn_clicked();

private:
    Ui::MainWindow *ui;

    void makeConnects();
};

#endif // MAINWINDOW_H
