#include "mainwindow.h"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile styleFile( "/home/avsimonenko/Documents/study/MAGA/1sem/AIS/repos/institute.matriculation.fri/matriculation.bc/gui/matriculation/res/custom.qss" );
    styleFile.open( QFile::ReadOnly );
    QString style( styleFile.readAll() );
    a.setStyleSheet(style);

    MainWindow w;
    w.showMaximized();

    return a.exec();
}
