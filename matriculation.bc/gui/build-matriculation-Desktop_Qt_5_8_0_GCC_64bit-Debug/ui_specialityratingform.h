/********************************************************************************
** Form generated from reading UI file 'specialityratingform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPECIALITYRATINGFORM_H
#define UI_SPECIALITYRATINGFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SpecialityRatingForm
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QCheckBox *checkBox;
    QLabel *nameLabel;
    QSpacerItem *horizontalSpacer;
    QTableWidget *tableWidget;

    void setupUi(QWidget *SpecialityRatingForm)
    {
        if (SpecialityRatingForm->objectName().isEmpty())
            SpecialityRatingForm->setObjectName(QStringLiteral("SpecialityRatingForm"));
        SpecialityRatingForm->resize(916, 589);
        verticalLayout = new QVBoxLayout(SpecialityRatingForm);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        checkBox = new QCheckBox(SpecialityRatingForm);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setIconSize(QSize(35, 35));

        horizontalLayout->addWidget(checkBox);

        nameLabel = new QLabel(SpecialityRatingForm);
        nameLabel->setObjectName(QStringLiteral("nameLabel"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Hairline"));
        font.setPointSize(25);
        font.setBold(true);
        font.setWeight(75);
        nameLabel->setFont(font);

        horizontalLayout->addWidget(nameLabel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        tableWidget = new QTableWidget(SpecialityRatingForm);
        if (tableWidget->columnCount() < 4)
            tableWidget->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setMinimumSize(QSize(0, 200));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato Medium"));
        font1.setPointSize(15);
        tableWidget->setFont(font1);
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSortingEnabled(false);
        tableWidget->horizontalHeader()->setStretchLastSection(false);
        tableWidget->verticalHeader()->setVisible(false);

        verticalLayout->addWidget(tableWidget);


        retranslateUi(SpecialityRatingForm);

        QMetaObject::connectSlotsByName(SpecialityRatingForm);
    } // setupUi

    void retranslateUi(QWidget *SpecialityRatingForm)
    {
        SpecialityRatingForm->setWindowTitle(QApplication::translate("SpecialityRatingForm", "Form", Q_NULLPTR));
        checkBox->setText(QString());
        nameLabel->setText(QApplication::translate("SpecialityRatingForm", "NAME", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("SpecialityRatingForm", "\320\235\320\276\320\274\320\265\321\200 \320\264\320\265\320\273\320\260", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("SpecialityRatingForm", "\320\244\320\230\320\236", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("SpecialityRatingForm", "\320\221\320\260\320\273\320\273", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("SpecialityRatingForm", "\320\236\321\200\320\270\320\263\320\270\320\275\320\260\320\273", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SpecialityRatingForm: public Ui_SpecialityRatingForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPECIALITYRATINGFORM_H
