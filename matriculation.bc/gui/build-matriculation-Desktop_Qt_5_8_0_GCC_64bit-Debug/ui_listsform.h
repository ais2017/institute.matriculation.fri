/********************************************************************************
** Form generated from reading UI file 'listsform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LISTSFORM_H
#define UI_LISTSFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ListsForm
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *backBtn;
    QSpacerItem *horizontalSpacer;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *listLayout;

    void setupUi(QWidget *ListsForm)
    {
        if (ListsForm->objectName().isEmpty())
            ListsForm->setObjectName(QStringLiteral("ListsForm"));
        ListsForm->resize(1015, 655);
        verticalLayout = new QVBoxLayout(ListsForm);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        backBtn = new QPushButton(ListsForm);
        backBtn->setObjectName(QStringLiteral("backBtn"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Hairline"));
        font.setPointSize(30);
        font.setBold(true);
        font.setWeight(75);
        backBtn->setFont(font);
        backBtn->setStyleSheet(QStringLiteral(""));

        horizontalLayout_7->addWidget(backBtn);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_7);

        scrollArea = new QScrollArea(ListsForm);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 995, 571));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        listLayout = new QVBoxLayout();
        listLayout->setObjectName(QStringLiteral("listLayout"));

        verticalLayout_2->addLayout(listLayout);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout->addWidget(scrollArea);


        retranslateUi(ListsForm);

        QMetaObject::connectSlotsByName(ListsForm);
    } // setupUi

    void retranslateUi(QWidget *ListsForm)
    {
        ListsForm->setWindowTitle(QApplication::translate("ListsForm", "Form", Q_NULLPTR));
        backBtn->setText(QApplication::translate("ListsForm", "\320\235\320\220 \320\223\320\233\320\220\320\222\320\235\320\243\320\256", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ListsForm: public Ui_ListsForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LISTSFORM_H
