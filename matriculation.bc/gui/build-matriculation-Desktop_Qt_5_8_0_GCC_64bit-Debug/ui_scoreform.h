/********************************************************************************
** Form generated from reading UI file 'scoreform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCOREFORM_H
#define UI_SCOREFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ScoreForm
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *backBtn;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QVBoxLayout *verticalLayout;
    QLineEdit *numLineEdit;
    QLineEdit *nameLineEdit;
    QLineEdit *scoreLineEdit;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *saveBtn;
    QSpacerItem *horizontalSpacer_2;
    QLabel *errorLabel;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *ScoreForm)
    {
        if (ScoreForm->objectName().isEmpty())
            ScoreForm->setObjectName(QStringLiteral("ScoreForm"));
        ScoreForm->resize(1059, 475);
        verticalLayout_3 = new QVBoxLayout(ScoreForm);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        backBtn = new QPushButton(ScoreForm);
        backBtn->setObjectName(QStringLiteral("backBtn"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Hairline"));
        font.setPointSize(30);
        font.setBold(true);
        font.setWeight(75);
        backBtn->setFont(font);
        backBtn->setStyleSheet(QStringLiteral(""));

        horizontalLayout_7->addWidget(backBtn);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout_7);

        verticalSpacer_2 = new QSpacerItem(20, 24, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(20);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(20);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label = new QLabel(ScoreForm);
        label->setObjectName(QStringLiteral("label"));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato Medium"));
        font1.setPointSize(15);
        font1.setBold(false);
        font1.setWeight(50);
        label->setFont(font1);

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(ScoreForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);

        verticalLayout_2->addWidget(label_2);

        label_3 = new QLabel(ScoreForm);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);

        verticalLayout_2->addWidget(label_3);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(20);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        numLineEdit = new QLineEdit(ScoreForm);
        numLineEdit->setObjectName(QStringLiteral("numLineEdit"));
        numLineEdit->setFont(font1);

        verticalLayout->addWidget(numLineEdit);

        nameLineEdit = new QLineEdit(ScoreForm);
        nameLineEdit->setObjectName(QStringLiteral("nameLineEdit"));
        nameLineEdit->setFont(font1);

        verticalLayout->addWidget(nameLineEdit);

        scoreLineEdit = new QLineEdit(ScoreForm);
        scoreLineEdit->setObjectName(QStringLiteral("scoreLineEdit"));
        scoreLineEdit->setFont(font1);

        verticalLayout->addWidget(scoreLineEdit);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 24, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, -1, 0);
        saveBtn = new QPushButton(ScoreForm);
        saveBtn->setObjectName(QStringLiteral("saveBtn"));
        saveBtn->setFont(font);

        horizontalLayout_2->addWidget(saveBtn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        errorLabel = new QLabel(ScoreForm);
        errorLabel->setObjectName(QStringLiteral("errorLabel"));
        errorLabel->setFont(font);

        horizontalLayout_2->addWidget(errorLabel);


        verticalLayout_3->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 114, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        verticalLayout_3->setStretch(2, 1);
        verticalLayout_3->setStretch(5, 1);

        retranslateUi(ScoreForm);

        QMetaObject::connectSlotsByName(ScoreForm);
    } // setupUi

    void retranslateUi(QWidget *ScoreForm)
    {
        ScoreForm->setWindowTitle(QApplication::translate("ScoreForm", "Form", Q_NULLPTR));
        backBtn->setText(QApplication::translate("ScoreForm", "\320\235\320\220 \320\223\320\233\320\220\320\222\320\235\320\243\320\256", Q_NULLPTR));
        label->setText(QApplication::translate("ScoreForm", "\320\235\320\276\320\274\320\265\321\200 \320\273\320\270\321\207\320\275\320\276\320\263\320\276 \320\264\320\265\320\273\320\260", Q_NULLPTR));
        label_2->setText(QApplication::translate("ScoreForm", "\320\230\321\201\320\277\321\213\321\202\320\260\320\275\320\270\320\265", Q_NULLPTR));
        label_3->setText(QApplication::translate("ScoreForm", "\320\221\320\260\320\273\320\273", Q_NULLPTR));
        saveBtn->setText(QApplication::translate("ScoreForm", "\320\241\320\236\320\245\320\240\320\220\320\235\320\230\320\242\320\254", Q_NULLPTR));
        errorLabel->setText(QApplication::translate("ScoreForm", "\320\235\320\225\320\232\320\236\320\240\320\240\320\225\320\232\320\242\320\235\320\220\320\257 \320\230\320\235\320\244\320\236\320\240\320\234\320\220\320\246\320\230\320\257", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ScoreForm: public Ui_ScoreForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCOREFORM_H
