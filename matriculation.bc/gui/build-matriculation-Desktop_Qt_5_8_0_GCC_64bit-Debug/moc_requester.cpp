/****************************************************************************
** Meta object code from reading C++ file 'requester.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../matriculation/controller/requester.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'requester.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Requester_t {
    QByteArrayData data[8];
    char stringdata0[103];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Requester_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Requester_t qt_meta_stringdata_Requester = {
    {
QT_MOC_LITERAL(0, 0, 9), // "Requester"
QT_MOC_LITERAL(1, 10, 22), // "finishAddPersonellFile"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 3), // "res"
QT_MOC_LITERAL(4, 38, 14), // "finishAddScore"
QT_MOC_LITERAL(5, 53, 14), // "finishGetLists"
QT_MOC_LITERAL(6, 68, 19), // "finishGetOrderLists"
QT_MOC_LITERAL(7, 88, 14) // "finishSetQuota"

    },
    "Requester\0finishAddPersonellFile\0\0res\0"
    "finishAddScore\0finishGetLists\0"
    "finishGetOrderLists\0finishSetQuota"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Requester[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       4,    1,   42,    2, 0x06 /* Public */,
       5,    0,   45,    2, 0x06 /* Public */,
       6,    0,   46,    2, 0x06 /* Public */,
       7,    1,   47,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    3,

       0        // eod
};

void Requester::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Requester *_t = static_cast<Requester *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finishAddPersonellFile((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->finishAddScore((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->finishGetLists(); break;
        case 3: _t->finishGetOrderLists(); break;
        case 4: _t->finishSetQuota((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Requester::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Requester::finishAddPersonellFile)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Requester::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Requester::finishAddScore)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Requester::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Requester::finishGetLists)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Requester::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Requester::finishGetOrderLists)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (Requester::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Requester::finishSetQuota)) {
                *result = 4;
                return;
            }
        }
    }
}

const QMetaObject Requester::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Requester.data,
      qt_meta_data_Requester,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Requester::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Requester::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Requester.stringdata0))
        return static_cast<void*>(const_cast< Requester*>(this));
    return QObject::qt_metacast(_clname);
}

int Requester::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void Requester::finishAddPersonellFile(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Requester::finishAddScore(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Requester::finishGetLists()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void Requester::finishGetOrderLists()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void Requester::finishSetQuota(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
