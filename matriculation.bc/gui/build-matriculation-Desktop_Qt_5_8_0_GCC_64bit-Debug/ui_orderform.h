/********************************************************************************
** Form generated from reading UI file 'orderform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORDERFORM_H
#define UI_ORDERFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OrderForm
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *backBtn;
    QSpacerItem *horizontalSpacer;
    QPlainTextEdit *plainTextEdit;
    QLabel *label;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *listLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *saveBtn;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *OrderForm)
    {
        if (OrderForm->objectName().isEmpty())
            OrderForm->setObjectName(QStringLiteral("OrderForm"));
        OrderForm->resize(974, 629);
        verticalLayout = new QVBoxLayout(OrderForm);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        backBtn = new QPushButton(OrderForm);
        backBtn->setObjectName(QStringLiteral("backBtn"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Hairline"));
        font.setPointSize(30);
        font.setBold(true);
        font.setWeight(75);
        backBtn->setFont(font);
        backBtn->setStyleSheet(QStringLiteral(""));

        horizontalLayout_7->addWidget(backBtn);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_7);

        plainTextEdit = new QPlainTextEdit(OrderForm);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato Medium"));
        font1.setPointSize(15);
        plainTextEdit->setFont(font1);

        verticalLayout->addWidget(plainTextEdit);

        label = new QLabel(OrderForm);
        label->setObjectName(QStringLiteral("label"));
        QFont font2;
        font2.setFamily(QStringLiteral("Lato Hairline"));
        font2.setPointSize(20);
        font2.setBold(true);
        font2.setWeight(75);
        label->setFont(font2);

        verticalLayout->addWidget(label);

        scrollArea = new QScrollArea(OrderForm);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 954, 244));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        listLayout = new QVBoxLayout();
        listLayout->setObjectName(QStringLiteral("listLayout"));

        verticalLayout_2->addLayout(listLayout);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout->addWidget(scrollArea);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 0);
        saveBtn = new QPushButton(OrderForm);
        saveBtn->setObjectName(QStringLiteral("saveBtn"));
        saveBtn->setFont(font);

        horizontalLayout->addWidget(saveBtn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        verticalLayout->setStretch(3, 1);

        retranslateUi(OrderForm);

        QMetaObject::connectSlotsByName(OrderForm);
    } // setupUi

    void retranslateUi(QWidget *OrderForm)
    {
        OrderForm->setWindowTitle(QApplication::translate("OrderForm", "Form", Q_NULLPTR));
        backBtn->setText(QApplication::translate("OrderForm", "\320\235\320\220 \320\223\320\233\320\220\320\222\320\235\320\243\320\256", Q_NULLPTR));
        plainTextEdit->setPlainText(QString());
        plainTextEdit->setPlaceholderText(QApplication::translate("OrderForm", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\202\320\265\320\272\321\201\321\202 \320\277\321\200\320\270\320\272\320\260\320\267\320\260", Q_NULLPTR));
        label->setText(QApplication::translate("OrderForm", "\320\222\321\213\320\261\320\265\321\200\320\265\321\202\320\265 \321\201\320\277\320\265\321\206\320\270\320\260\320\273\321\214\320\275\320\276\321\201\321\202\320\270, \320\272\320\276\321\202\320\276\321\200\321\213\320\265 \320\261\321\203\320\264\321\203\321\202 \320\262\320\272\320\273\321\216\321\207\320\265\320\275\321\213 \320\262 \320\277\321\200\320\270\320\272\320\260\320\267:", Q_NULLPTR));
        saveBtn->setText(QApplication::translate("OrderForm", "\320\241\320\236\320\245\320\240\320\220\320\235\320\230\320\242\320\254", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class OrderForm: public Ui_OrderForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORDERFORM_H
