/********************************************************************************
** Form generated from reading UI file 'quotaform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QUOTAFORM_H
#define UI_QUOTAFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QuotaForm
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *backBtn;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QVBoxLayout *verticalLayout;
    QComboBox *comboBoxSpec;
    QLineEdit *lineEditQuota;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *saveBtn;
    QSpacerItem *horizontalSpacer_2;
    QLabel *errorLabel;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *QuotaForm)
    {
        if (QuotaForm->objectName().isEmpty())
            QuotaForm->setObjectName(QStringLiteral("QuotaForm"));
        QuotaForm->resize(877, 540);
        verticalLayout_3 = new QVBoxLayout(QuotaForm);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        backBtn = new QPushButton(QuotaForm);
        backBtn->setObjectName(QStringLiteral("backBtn"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Hairline"));
        font.setPointSize(30);
        font.setBold(true);
        font.setWeight(75);
        backBtn->setFont(font);
        backBtn->setStyleSheet(QStringLiteral(""));

        horizontalLayout_7->addWidget(backBtn);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout_7);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(20);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(20);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label = new QLabel(QuotaForm);
        label->setObjectName(QStringLiteral("label"));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato Medium"));
        font1.setPointSize(15);
        label->setFont(font1);

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(QuotaForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);

        verticalLayout_2->addWidget(label_2);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(20);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        comboBoxSpec = new QComboBox(QuotaForm);
        comboBoxSpec->setObjectName(QStringLiteral("comboBoxSpec"));
        comboBoxSpec->setMinimumSize(QSize(0, 35));

        verticalLayout->addWidget(comboBoxSpec);

        lineEditQuota = new QLineEdit(QuotaForm);
        lineEditQuota->setObjectName(QStringLiteral("lineEditQuota"));
        lineEditQuota->setFont(font1);

        verticalLayout->addWidget(lineEditQuota);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, -1, 0);
        saveBtn = new QPushButton(QuotaForm);
        saveBtn->setObjectName(QStringLiteral("saveBtn"));
        saveBtn->setFont(font);

        horizontalLayout_2->addWidget(saveBtn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        errorLabel = new QLabel(QuotaForm);
        errorLabel->setObjectName(QStringLiteral("errorLabel"));
        errorLabel->setFont(font);

        horizontalLayout_2->addWidget(errorLabel);


        verticalLayout_3->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 142, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        verticalLayout_3->setStretch(2, 1);
        verticalLayout_3->setStretch(5, 1);

        retranslateUi(QuotaForm);

        QMetaObject::connectSlotsByName(QuotaForm);
    } // setupUi

    void retranslateUi(QWidget *QuotaForm)
    {
        QuotaForm->setWindowTitle(QApplication::translate("QuotaForm", "Form", Q_NULLPTR));
        backBtn->setText(QApplication::translate("QuotaForm", "\320\235\320\220 \320\223\320\233\320\220\320\222\320\235\320\243\320\256", Q_NULLPTR));
        label->setText(QApplication::translate("QuotaForm", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265 \321\201\320\277\320\265\321\206\320\270\320\260\320\273\321\214\320\275\320\276\321\201\321\202\320\270", Q_NULLPTR));
        label_2->setText(QApplication::translate("QuotaForm", "\320\232\320\262\320\276\321\202\320\260", Q_NULLPTR));
        saveBtn->setText(QApplication::translate("QuotaForm", "\320\241\320\236\320\245\320\240\320\220\320\235\320\230\320\242\320\254", Q_NULLPTR));
        errorLabel->setText(QApplication::translate("QuotaForm", "\320\235\320\225\320\232\320\236\320\240\320\240\320\225\320\232\320\242\320\235\320\220\320\257 \320\230\320\235\320\244\320\236\320\240\320\234\320\220\320\246\320\230\320\257", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class QuotaForm: public Ui_QuotaForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QUOTAFORM_H
