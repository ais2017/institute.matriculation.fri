/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "view/listsform.h"
#include "view/orderform.h"
#include "view/quotaform.h"
#include "view/scoreform.h"
#include "view/studentinfoform.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_6;
    QWidget *mainPageWidget;
    QVBoxLayout *verticalLayout_7;
    QSpacerItem *verticalSpacer;
    QLabel *label_6;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_11;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *studentInfoBtn;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *scoreBtn;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *quotaBtn;
    QSpacerItem *horizontalSpacer_6;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_13;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *listsBtn;
    QSpacerItem *horizontalSpacer_10;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *orderBtn;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_14;
    QSpacerItem *verticalSpacer_2;
    OrderForm *orderWidget;
    StudentInfoForm *studentInfoWidget;
    ListsForm *listsWidget;
    ScoreForm *scoreWidget;
    QuotaForm *quotaWidget;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1070, 713);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QLatin1String("background: white;\n"
""));
        verticalLayout_6 = new QVBoxLayout(centralWidget);
        verticalLayout_6->setSpacing(20);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(-1, 20, -1, -1);
        mainPageWidget = new QWidget(centralWidget);
        mainPageWidget->setObjectName(QStringLiteral("mainPageWidget"));
        verticalLayout_7 = new QVBoxLayout(mainPageWidget);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(-1, 10, -1, -1);
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        label_6 = new QLabel(mainPageWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Hairline"));
        font.setPointSize(40);
        font.setBold(true);
        font.setWeight(75);
        label_6->setFont(font);
        label_6->setStyleSheet(QStringLiteral("background: none; color: #4286f4"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(label_6);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_3);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, -1, 0, -1);
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_11);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        studentInfoBtn = new QPushButton(mainPageWidget);
        studentInfoBtn->setObjectName(QStringLiteral("studentInfoBtn"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(studentInfoBtn->sizePolicy().hasHeightForWidth());
        studentInfoBtn->setSizePolicy(sizePolicy);
        studentInfoBtn->setMinimumSize(QSize(160, 160));
        studentInfoBtn->setLayoutDirection(Qt::LeftToRight);
        studentInfoBtn->setStyleSheet(QStringLiteral("border: none; background: none"));

        horizontalLayout->addWidget(studentInfoBtn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        label = new QLabel(mainPageWidget);
        label->setObjectName(QStringLiteral("label"));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato Medium"));
        font1.setPointSize(15);
        label->setFont(font1);
        label->setLayoutDirection(Qt::LeftToRight);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        verticalLayout->setStretch(0, 5);
        verticalLayout->setStretch(1, 3);

        horizontalLayout_7->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        scoreBtn = new QPushButton(mainPageWidget);
        scoreBtn->setObjectName(QStringLiteral("scoreBtn"));
        sizePolicy.setHeightForWidth(scoreBtn->sizePolicy().hasHeightForWidth());
        scoreBtn->setSizePolicy(sizePolicy);
        scoreBtn->setMinimumSize(QSize(160, 160));
        scoreBtn->setLayoutDirection(Qt::LeftToRight);
        scoreBtn->setStyleSheet(QStringLiteral("border: none; background: none"));

        horizontalLayout_2->addWidget(scoreBtn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout_2);

        label_2 = new QLabel(mainPageWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);
        label_2->setLayoutDirection(Qt::LeftToRight);
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_2);

        verticalLayout_2->setStretch(0, 5);
        verticalLayout_2->setStretch(1, 3);

        horizontalLayout_7->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        quotaBtn = new QPushButton(mainPageWidget);
        quotaBtn->setObjectName(QStringLiteral("quotaBtn"));
        sizePolicy.setHeightForWidth(quotaBtn->sizePolicy().hasHeightForWidth());
        quotaBtn->setSizePolicy(sizePolicy);
        quotaBtn->setMinimumSize(QSize(160, 160));
        quotaBtn->setLayoutDirection(Qt::LeftToRight);
        quotaBtn->setStyleSheet(QStringLiteral("border: none; background: none"));

        horizontalLayout_3->addWidget(quotaBtn);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        verticalLayout_3->addLayout(horizontalLayout_3);

        label_3 = new QLabel(mainPageWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);
        label_3->setLayoutDirection(Qt::LeftToRight);
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_3);

        verticalLayout_3->setStretch(0, 5);
        verticalLayout_3->setStretch(1, 3);

        horizontalLayout_7->addLayout(verticalLayout_3);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_12);


        verticalLayout_7->addLayout(horizontalLayout_7);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_4);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, -1, 0, -1);
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_13);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_9);

        listsBtn = new QPushButton(mainPageWidget);
        listsBtn->setObjectName(QStringLiteral("listsBtn"));
        sizePolicy.setHeightForWidth(listsBtn->sizePolicy().hasHeightForWidth());
        listsBtn->setSizePolicy(sizePolicy);
        listsBtn->setMinimumSize(QSize(160, 160));
        listsBtn->setLayoutDirection(Qt::LeftToRight);
        listsBtn->setStyleSheet(QStringLiteral("border: none; background: none"));

        horizontalLayout_5->addWidget(listsBtn);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_10);


        verticalLayout_5->addLayout(horizontalLayout_5);

        label_5 = new QLabel(mainPageWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font1);
        label_5->setLayoutDirection(Qt::LeftToRight);
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(label_5);

        verticalLayout_5->setStretch(0, 5);
        verticalLayout_5->setStretch(1, 3);

        horizontalLayout_6->addLayout(verticalLayout_5);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        orderBtn = new QPushButton(mainPageWidget);
        orderBtn->setObjectName(QStringLiteral("orderBtn"));
        sizePolicy.setHeightForWidth(orderBtn->sizePolicy().hasHeightForWidth());
        orderBtn->setSizePolicy(sizePolicy);
        orderBtn->setMinimumSize(QSize(160, 160));
        orderBtn->setLayoutDirection(Qt::LeftToRight);
        orderBtn->setStyleSheet(QStringLiteral("border: none; background: none"));

        horizontalLayout_4->addWidget(orderBtn);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        verticalLayout_4->addLayout(horizontalLayout_4);

        label_4 = new QLabel(mainPageWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font1);
        label_4->setLayoutDirection(Qt::LeftToRight);
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_4);

        verticalLayout_4->setStretch(0, 5);
        verticalLayout_4->setStretch(1, 3);

        horizontalLayout_6->addLayout(verticalLayout_4);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_14);


        verticalLayout_7->addLayout(horizontalLayout_6);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_2);


        verticalLayout_6->addWidget(mainPageWidget);

        orderWidget = new OrderForm(centralWidget);
        orderWidget->setObjectName(QStringLiteral("orderWidget"));

        verticalLayout_6->addWidget(orderWidget);

        studentInfoWidget = new StudentInfoForm(centralWidget);
        studentInfoWidget->setObjectName(QStringLiteral("studentInfoWidget"));
        label_6->raise();

        verticalLayout_6->addWidget(studentInfoWidget);

        listsWidget = new ListsForm(centralWidget);
        listsWidget->setObjectName(QStringLiteral("listsWidget"));

        verticalLayout_6->addWidget(listsWidget);

        scoreWidget = new ScoreForm(centralWidget);
        scoreWidget->setObjectName(QStringLiteral("scoreWidget"));

        verticalLayout_6->addWidget(scoreWidget);

        quotaWidget = new QuotaForm(centralWidget);
        quotaWidget->setObjectName(QStringLiteral("quotaWidget"));

        verticalLayout_6->addWidget(quotaWidget);

        MainWindow->setCentralWidget(centralWidget);
        studentInfoBtn->raise();
        orderBtn->raise();
        label_4->raise();
        studentInfoWidget->raise();
        scoreWidget->raise();
        mainPageWidget->raise();
        listsWidget->raise();
        quotaWidget->raise();
        orderWidget->raise();
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1070, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "\320\241\320\230\320\241\320\242\320\225\320\234\320\220 \320\237\320\240\320\230\320\225\320\234\320\235\320\236\320\231 \320\232\320\236\320\234\320\230\320\241\320\241\320\230\320\230", Q_NULLPTR));
        studentInfoBtn->setText(QString());
        label->setText(QApplication::translate("MainWindow", "\320\230\320\267\320\274\320\265\320\275\320\270\321\202\321\214 \320\264\320\260\320\275\320\275\321\213\320\265 \320\276 \321\201\321\202\321\203\320\264\320\265\320\275\321\202\320\265", Q_NULLPTR));
        scoreBtn->setText(QString());
        label_2->setText(QApplication::translate("MainWindow", "\320\230\320\267\320\274\320\265\320\275\320\270\321\202\321\214 \320\264\320\260\320\275\320\275\321\213\320\265 \320\276 \n"
"\320\272\320\276\320\275\320\272\321\203\321\200\321\201\320\275\321\213\321\205 \320\261\320\260\320\273\320\273\320\260\321\205 \321\201\321\202\321\203\320\264\320\265\320\275\321\202\320\260", Q_NULLPTR));
        quotaBtn->setText(QString());
        label_3->setText(QApplication::translate("MainWindow", "\320\230\320\267\320\274\320\265\320\275\320\270\321\202\321\214 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\272\320\262\320\276\321\202\321\213", Q_NULLPTR));
        listsBtn->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "\320\237\320\276\320\273\321\203\321\207\320\270\321\202\321\214 \321\201\320\277\320\270\321\201\320\272\320\270", Q_NULLPTR));
        orderBtn->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "\320\241\321\204\320\276\321\200\320\274\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\277\321\200\320\270\320\272\320\260\320\267\321\213", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
