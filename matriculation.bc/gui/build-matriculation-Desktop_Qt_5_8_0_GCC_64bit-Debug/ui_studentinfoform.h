/********************************************************************************
** Form generated from reading UI file 'studentinfoform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STUDENTINFOFORM_H
#define UI_STUDENTINFOFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StudentInfoForm
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *backBtn;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEditFname;
    QComboBox *specComboBox;
    QLineEdit *lineEditFilial;
    QLineEdit *lineEditDocNum;
    QLineEdit *lineEditEducation;
    QCheckBox *checkBox;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *saveBtn;
    QSpacerItem *horizontalSpacer_2;
    QLabel *errorLabel;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *StudentInfoForm)
    {
        if (StudentInfoForm->objectName().isEmpty())
            StudentInfoForm->setObjectName(QStringLiteral("StudentInfoForm"));
        StudentInfoForm->resize(971, 713);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(StudentInfoForm->sizePolicy().hasHeightForWidth());
        StudentInfoForm->setSizePolicy(sizePolicy);
        StudentInfoForm->setStyleSheet(QStringLiteral(""));
        verticalLayout_3 = new QVBoxLayout(StudentInfoForm);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        backBtn = new QPushButton(StudentInfoForm);
        backBtn->setObjectName(QStringLiteral("backBtn"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Hairline"));
        font.setPointSize(30);
        font.setBold(true);
        font.setWeight(75);
        backBtn->setFont(font);
        backBtn->setStyleSheet(QStringLiteral(""));

        horizontalLayout_7->addWidget(backBtn);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout_7);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(20);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(20);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label = new QLabel(StudentInfoForm);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(0, 35));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato Medium"));
        font1.setPointSize(15);
        label->setFont(font1);

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(StudentInfoForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(0, 35));
        label_2->setFont(font1);

        verticalLayout_2->addWidget(label_2);

        label_3 = new QLabel(StudentInfoForm);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(0, 35));
        label_3->setFont(font1);

        verticalLayout_2->addWidget(label_3);

        label_4 = new QLabel(StudentInfoForm);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(0, 35));
        label_4->setFont(font1);

        verticalLayout_2->addWidget(label_4);

        label_5 = new QLabel(StudentInfoForm);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(0, 35));
        label_5->setFont(font1);

        verticalLayout_2->addWidget(label_5);

        label_6 = new QLabel(StudentInfoForm);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(0, 35));
        label_6->setFont(font1);

        verticalLayout_2->addWidget(label_6);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(20);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lineEditFname = new QLineEdit(StudentInfoForm);
        lineEditFname->setObjectName(QStringLiteral("lineEditFname"));
        lineEditFname->setMinimumSize(QSize(0, 35));
        lineEditFname->setFont(font1);

        verticalLayout->addWidget(lineEditFname);

        specComboBox = new QComboBox(StudentInfoForm);
        specComboBox->setObjectName(QStringLiteral("specComboBox"));
        specComboBox->setMinimumSize(QSize(0, 35));

        verticalLayout->addWidget(specComboBox);

        lineEditFilial = new QLineEdit(StudentInfoForm);
        lineEditFilial->setObjectName(QStringLiteral("lineEditFilial"));
        lineEditFilial->setMinimumSize(QSize(0, 35));
        lineEditFilial->setFont(font1);

        verticalLayout->addWidget(lineEditFilial);

        lineEditDocNum = new QLineEdit(StudentInfoForm);
        lineEditDocNum->setObjectName(QStringLiteral("lineEditDocNum"));
        lineEditDocNum->setMinimumSize(QSize(0, 35));
        lineEditDocNum->setFont(font1);

        verticalLayout->addWidget(lineEditDocNum);

        lineEditEducation = new QLineEdit(StudentInfoForm);
        lineEditEducation->setObjectName(QStringLiteral("lineEditEducation"));
        lineEditEducation->setMinimumSize(QSize(0, 35));
        lineEditEducation->setFont(font1);

        verticalLayout->addWidget(lineEditEducation);

        checkBox = new QCheckBox(StudentInfoForm);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setMinimumSize(QSize(0, 35));
        checkBox->setBaseSize(QSize(35, 35));
        checkBox->setStyleSheet(QStringLiteral(""));
        checkBox->setIconSize(QSize(35, 35));

        verticalLayout->addWidget(checkBox);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, -1, 0);
        saveBtn = new QPushButton(StudentInfoForm);
        saveBtn->setObjectName(QStringLiteral("saveBtn"));
        saveBtn->setFont(font);

        horizontalLayout_2->addWidget(saveBtn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        errorLabel = new QLabel(StudentInfoForm);
        errorLabel->setObjectName(QStringLiteral("errorLabel"));
        errorLabel->setFont(font);

        horizontalLayout_2->addWidget(errorLabel);


        verticalLayout_3->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        verticalLayout_3->setStretch(2, 1);
        verticalLayout_3->setStretch(5, 1);

        retranslateUi(StudentInfoForm);

        QMetaObject::connectSlotsByName(StudentInfoForm);
    } // setupUi

    void retranslateUi(QWidget *StudentInfoForm)
    {
        StudentInfoForm->setWindowTitle(QApplication::translate("StudentInfoForm", "Form", Q_NULLPTR));
        backBtn->setText(QApplication::translate("StudentInfoForm", "\320\235\320\220 \320\223\320\233\320\220\320\222\320\235\320\243\320\256", Q_NULLPTR));
        label->setText(QApplication::translate("StudentInfoForm", "\320\244\320\230\320\236", Q_NULLPTR));
        label_2->setText(QApplication::translate("StudentInfoForm", "\320\235\320\260\320\277\321\200\320\260\320\262\320\273\320\265\320\275\320\270\320\265 \320\277\320\276\320\264\320\263\320\276\321\202\320\276\320\262\320\272\320\270", Q_NULLPTR));
        label_3->setText(QApplication::translate("StudentInfoForm", "\320\244\320\270\320\273\320\270\320\260\320\273", Q_NULLPTR));
        label_4->setText(QApplication::translate("StudentInfoForm", "\342\204\226 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202\320\260 \320\276\320\261 \320\276\320\261\321\200\320\260\320\267\320\276\320\262\320\260\320\275\320\270\320\270", Q_NULLPTR));
        label_5->setText(QApplication::translate("StudentInfoForm", "\320\237\321\200\320\265\320\264\321\213\320\264\321\203\321\211\320\265\320\265 \320\274\320\265\321\201\321\202\320\276 \320\276\320\261\321\203\321\207\320\265\320\275\320\270\321\217", Q_NULLPTR));
        label_6->setText(QApplication::translate("StudentInfoForm", "\320\236\321\200\320\270\320\263\320\270\320\275\320\260\320\273", Q_NULLPTR));
        checkBox->setText(QString());
        saveBtn->setText(QApplication::translate("StudentInfoForm", "\320\241\320\236\320\245\320\240\320\220\320\235\320\230\320\242\320\254", Q_NULLPTR));
        errorLabel->setText(QApplication::translate("StudentInfoForm", "\320\235\320\225\320\232\320\236\320\240\320\240\320\225\320\232\320\242\320\235\320\220\320\257 \320\230\320\235\320\244\320\236\320\240\320\234\320\220\320\246\320\230\320\257", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class StudentInfoForm: public Ui_StudentInfoForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STUDENTINFOFORM_H
