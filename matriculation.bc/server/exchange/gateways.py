import tests.samples as sa
import random

from matriculation.domain.personellfile import Examination, PersonellFile
from matriculation.domain.admission import Order


class DBGateway(object):
    def __init__(self):
        pass

    # check syntax of select and then make request to database
    def getListSelectWithArgs(self, spec=None, quota=None, minsum=None):
        if spec != None and len(spec) == 0:
            return None

        if quota != None and quota <= 0:
            return None

        if spec and quota and minsum == None:
            return None

        if spec and not isinstance(spec, str):
            raise TypeError

        if quota and not isinstance(quota, int):
            raise TypeError

        if minsum and not isinstance(minsum, int):
            raise TypeError

        return sa.createSampleRating(spec, quota, minsum)

    def sendExam(self, data):
        if len(data) != 2:
            raise ValueError

        if not isinstance(data[0], int):
            raise TypeError

        if not isinstance(data[1], Examination):
            raise TypeError

        if self.examExists(
                data):  # may be swithched to fileExists(data[0]) method, but I need True result, so I implement this
            return True
        else:
            return False

    def getAdmission(self):
        return sa.createSampleOrder()

    def uploadAdmission(self, order):
        if not isinstance(order, Order):
            raise TypeError

        return True

    def sendFile(self, file):
        if not isinstance(file, PersonellFile):
            raise TypeError

        if self.fileExists(file.number):
            return False
        return True

    def getFile(self, ide):
        if not isinstance(ide, int):
            raise TypeError
        if self.fileExists(ide):
            return sa.createSampleFile(ide)
        return None

    def fileExists(self, num):
        return False

    def examExists(self, data):
        return True

    def updateQuota(self, data):
        if len(data) != 2:
            raise ValueError

        if not isinstance(data[0], str):
            raise TypeError

        if not isinstance(data[1], int):
            raise TypeError

        if data[1] <= 0:
            raise ValueError

        if self.specExists(data[0]):
            return True
        else:
            return False

    def specExists(self, name):
        return True


class InterfaceGateway(object):
    def getFile(self):
        return sa.createSampleFile()

    def getQuotaPair(self):
        spec = "dddd"
        quota = random.randint(1, 70)
        return [spec, quota]

    def getExamPair(self):
        file = random.randint(1024, 2048)
        exam = sa.createSampleExam()
        return [file, exam]

    def getSpec(self):
        spec = "speciality"
        return spec

    def sendAdmission(self, orderData):
        if not isinstance(orderData, Order):
            raise TypeError

        return True

    def sendFile(self, file):
        if not isinstance(file, PersonellFile):
            raise TypeError
        return True
