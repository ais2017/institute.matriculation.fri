from exchange.gateways import DBGateway
import psycopg2
from matriculation.domain.personellfile import *
from matriculation.domain.rating import *
from matriculation.domain.speciality import *


class DataBaseGateway(DBGateway):
    def __init__(self):
        DBGateway.__init__(DBGateway)
        try:
            self.conn = psycopg2.connect(dbname="postgres", user="postgres", password="postgres")
        except:
            print("I am unable to connect to the database.")

    def getAllSpecialities(self):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM speciality;")
        rows = cur.fetchall()
        return rows

    def sendFile(self, file):
        if not isinstance(file, PersonellFile):
            raise TypeError

        cur = self.conn.cursor()
        cur.execute("SELECT MAX(ID) FROM personell_file;")
        max_id = cur.fetchall()[0][0]
        if max_id is None:
            max_id = 0

        try:
            cur.execute("INSERT INTO personell_file VALUES (%d, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %r);" %
                        (max_id+1, file._applicant, file._course, file._branch, file._document,
                         file._education, file._original))
            self.conn.commit()
            return True
        except:
            return False

    def setScore(self, student_id: int, exam):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM exam WHERE (name=\'%s\' AND file_id=%d);" % (exam._name, student_id))
        res = cur.fetchall()

        try:
            if len(res) == 0:
                cur.execute("INSERT INTO exam VALUES (%d, \'%s\', %d);" %
                                       (student_id, exam._name, exam._score))
            else:
                cur.execute("UPDATE exam SET score = %d WHERE (name = \'%s\' AND file_id = %d);" %
                                       (exam._score, exam._name, student_id))
            self.conn.commit()
            return True
        except:
            return False

    def getLists(self):
        try:
            cur = self.conn.cursor()
            cur.execute("select * from speciality;")
            rows = cur.fetchall()
            specs = []
            for r in rows:
                cur_spec = Speciality(*r);
                specs.append(cur_spec)
                cur.execute("select * from personell_file where (spec_name=\'%s\');" % cur_spec.name)
                files = cur.fetchall()
                for f in files:
                    pf = PersonellFile(*f)
                    cur.execute("select name, score from exam where (file_id=%d);" % pf._number)
                    scores = cur.fetchall()
                    for scr in scores:
                        pf.addExam(scr[0])
                        pf.setExamScore(*scr)
                    print('pf score = ' + str(pf.exam_score))
                    cur_spec.rating.append(pf)
            return specs

        except:
            return None

    def getOrders(self):
        try:
            specs = self.getLists()
            order_specs = []
            for s in specs:
                pfs_fit = {}
                pfs = s._rating._files.values()
                for pf in pfs:
                    if pf._original:
                        print('pf.exam_score = ' + str(pf.exam_score))
                        pfs_fit[pf._number] = pf.exam_score

                    pfs_fit_s = sorted(pfs_fit.items(), key=operator.itemgetter(1), reverse=True)
                    limit = s._quota
                    if len(pfs_fit) < limit:
                        limit = len(pfs_fit)
                    pfs_fit_s = pfs_fit_s[:limit]
                    pfs_rating = {}
                    for tpl in pfs_fit_s:
                        pfs_rating[tpl[0]] = s._rating._files[tpl[0]]
                s._rating._files = pfs_rating

                order_specs.append(s)
            return order_specs
        except:
            return None

    def setQuota(self, sp: Speciality):
        try:
            cur = self.conn.cursor()
            cur.execute("UPDATE speciality SET quota = %d WHERE (name = \'%s\');" %
                        (sp._quota, sp._name))
            self.conn.commit()
            return True
        except:
            return False
