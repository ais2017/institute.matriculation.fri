from server_part.apiserver import *
from server_part.databasegateway import *

from matriculation.domain.personellfile import *
from matriculation.domain.speciality import *
from server_part.apiserver import ApiServer, ApiRoute

db_gateway = DataBaseGateway()


class MyServer(ApiServer):
    @ApiRoute("/add_personell_file")
    def add_personell_file(req):
        pf = PersonellFile(0, ' ', ' ', ' ', ' ', ' ', False)
        print(type(req))
        req['_number'] = 0
        print(req)
        req_str = str(req)
        req_str = req_str.replace('\'', '\"')
        req_str = req_str.replace('True', 'true')
        req_str = req_str.replace('False', 'false')
        print('req = ' + req_str)

        pf.fromJSON(req_str)
        if db_gateway.sendFile(pf):
            return {"result": "OK"}
        else:
            return {"result": "BAD"}

    @ApiRoute("/add_score")
    def add_score(req):
        ex = Examination(' ')
        req_str = str(req)
        req_str = req_str.replace('\'', '\"')
        req_str = req_str.replace('True', 'true')
        req_str = req_str.replace('False', 'false')
        ex.fromJSON(req_str)
        print('ex name = ' + str(ex._name))

        if db_gateway.setScore(req["id"], ex):
            return {"result": "OK"}
        else:
            return {"result": "BAD"}

    @ApiRoute("/set_quota")
    def set_quota(req):
        sp = Speciality(' ', 0)
        req_str = str(req)
        req_str = req_str.replace('\'', '\"')
        req_str = req_str.replace('True', 'true')
        req_str = req_str.replace('False', 'false')
        sp.fromJSON(req_str)

        if db_gateway.setQuota(sp):
            return {"result": "OK"}
        else:
            return {"result": "BAD"}

    @ApiRoute("/specialities")
    def get_specialities(req):
        if req:
             raise ApiError(501,"no data in for baz")
        rows = db_gateway.getAllSpecialities()
        sp_list = []
        for r in rows:
            sp_list.append(Speciality(*r).toJSON())
        return sp_list

    @ApiRoute("/get_lists")
    def get_lists(req):
        if req:
             raise ApiError(501,"no data in for baz")
        list = db_gateway.getLists()
        specs = []
        if list is not None:
            for s in list:
                specs.append(s.toJSON())
        return specs

    @ApiRoute("/get_lists_for_order")
    def get_lists_for_order(req):
        if req:
             raise ApiError(501,"no data in for baz")
        list = db_gateway.getOrders()
        specs = []
        if list is not None:
            for s in list:
                specs.append(s.toJSON())
        return specs

MyServer("127.0.0.1", 8000).serve_forever()

