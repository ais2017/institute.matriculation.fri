from exchange.gateways import DBGateway, InterfaceGateway

class UseCase(object):
    def __init__(self, db, intrfc):
        if not isinstance(db, DBGateway):
            raise TypeError
        if not isinstance(intrfc, InterfaceGateway):
            raise TypeError

        self._db = db
        self._intrfc = intrfc

class EditAdmission(UseCase):
    def execute(self):
        return self._db.updateQuota(self._intrfc.getQuotaPair())


class GetAdmission(UseCase):
    def execute(self):
        return self._intrfc.sendAdmission(self._db.getAdmission()) and self._db.uploadAdmission(self._db.getAdmission())


class GetLists(UseCase):
    def execute(self):
        return self._db.getListSelectWithArgs(self._intrfc.getSpec())

class Registration(UseCase):
    def execute(self, arg):
        if arg == "reg":
            return self._db.sendFile(self._intrfc.getFile())
        if arg == "exam":
            return self._db.sendExam(self._intrfc.getExamPair())

        return False