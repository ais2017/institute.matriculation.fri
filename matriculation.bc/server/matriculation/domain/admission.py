import datetime

import operator

from matriculation.domain.speciality import Speciality
from matriculation.domain.rating import Rating


class Order(object):
    def __init__(self, specs=None):
        self._date = datetime.date.today()
        self._text = None
        if specs == None:
            self.lists = dict()
        else:
            self.lists = specs

    date = property(operator.attrgetter('_date'))
    text = property(operator.attrgetter('_text'))
    lists = property(operator.attrgetter('_lists'))

    @lists.setter
    def lists(self, specs):
        if not isinstance(specs, dict):
            raise TypeError
        for spec in specs.values():
            spec.rating.final = True
        self._lists = specs

    def add_speciality(self, spec):
        if not isinstance(spec, Speciality):
            raise TypeError

        if spec.name not in self.lists.keys():
            self.lists[spec.name] = spec

    def set_quota(self, name, quota):
        if quota > 0:
            if name in self._lists.keys():
                self._lists[name].quota = quota
            else:
                raise ValueError
        else:
            raise ValueError

    def getFinal(self):
        return dict((spec.name, spec.final()) for spec in self.lists.values())

