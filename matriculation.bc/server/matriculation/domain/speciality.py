import operator

from matriculation.domain.rating import Rating
import json


class Speciality(object):
    def __init__(self, name, quota, rating = None):
        if not isinstance(name, str):
            raise TypeError
        self._name = name
        self._quota = quota
        if rating == None:
            self._rating = Rating()
        else:
            if not isinstance(rating, Rating):
                raise TypeError
            else:
                self._rating = rating

    name = property(operator.attrgetter('_name'))
    quota = property(operator.attrgetter('_quota'))
    rating = property(operator.attrgetter('_rating'))

    @quota.setter
    def quota(self, quota):
        if not isinstance(quota, int):
            raise TypeError("quota must be int value")
        if quota > 0:
            self._quota = quota
        else:
            raise ValueError

    def exam_score(self, item):
        return item.exam_score

    def final(self):
        ratingListBuf = sorted(self.rating.files.values(), key=self.exam_score)[0:self.quota]
        finalRating = Rating()
        for item in ratingListBuf:
            finalRating.append(item)
        result = Speciality(self.name, self.quota, finalRating)

        return result

    def toJSON(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4))

    def fromJSON(self, j):
        self.__dict__ = json.loads(j)