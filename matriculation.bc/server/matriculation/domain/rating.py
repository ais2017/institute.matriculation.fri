import operator

from matriculation.domain.personellfile import PersonellFile
import json

class Rating(object):
    # def __init__(self):
    #    self._list = []
    #    self._final = False

    def __init__(self, rating=None):
        if rating == None:
            self._files = dict()
            self._final = False
        else:
            if not isinstance(rating, Rating):
                raise TypeError

            self._files = rating.files
            self._final = rating.final

    files = property(operator.attrgetter('_files'))
    final = property(operator.attrgetter('_final'))

    @final.setter
    def final(self, boolean):
        self._final = True

    def append(self, file):
        if not isinstance(file, PersonellFile):
            raise TypeError

        if not self._final:
            if file.number not in self._files.keys():
                self._files[file.number] = file

    def remove(self, number):
        if not isinstance(number, int):
            raise TypeError
        if not self._final:
            self._files.pop(number)

    def toJSON(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__,
                                     sort_keys=True, indent=4))

    def fromJSON(self, j):
        self.__dict__ = json.loads(j)

    #def sum(self, pair):
    #    item = self.files[pair[0]]
    #    return item.exam_score
