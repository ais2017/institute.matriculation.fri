import operator
import json


class Examination(object):
    def __init__(self, name):
        self._name = name
        self._score = 0

    score = property(operator.attrgetter('_score'))
    name = property(operator.attrgetter('_name'))

    @score.setter
    def score(self, score):
        if not isinstance(score, int):
            raise TypeError("score must be int value")
        if (score >= 0):
            self._score = score
        else:
            raise ValueError

    @name.setter
    def name(self, name):
        if not isinstance(name, str):
            raise TypeError
        if name == "":
            raise ValueError
        self._name = name

    def toJSON(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4))

    def fromJSON(self, j):
        self.__dict__ = json.loads(j)


class PersonellFile(object):
    def __init__(self, number, applicant, course, branch, document, education, original):

        if not isinstance(number, int):
            raise TypeError("number must be int value")
        if not isinstance(applicant, str):
            raise TypeError
        if not isinstance(course, str):
            raise TypeError
        if not isinstance(branch, str):
            raise TypeError
        if not isinstance(document, str):
            raise TypeError
        if not isinstance(education, str):
            raise TypeError
        if not isinstance(original, bool):
            raise TypeError

        self._number = number
        self._applicant = applicant
        self._course = course
        self._branch = branch
        self._document = document
        self._education = education
        self._original = original
        self.exams = dict()
        self._exam_score = 0

    number = property(operator.attrgetter('_number'))
    course = property(operator.attrgetter('_course'))
    branch = property(operator.attrgetter('_branch'))
    applicant = property(operator.attrgetter('_applicant'))
    document = property(operator.attrgetter('_document'))
    education = property(operator.attrgetter('_education'))
    original = property(operator.attrgetter('_original'))
    exam_score = property(operator.attrgetter('_exam_score'))

    @exam_score.getter
    def exam_score(self):
        self._exam_score = sum(exam.score for exam in self.exams.values())
        return self._exam_score

    def setOriginal(self):
        self._original = True

    def unSetOriginal(self):
        self._original = False

    def addExam(self, name):
        if not isinstance(name, str):
            raise TypeError

        if not self.isExistedExam(name):
            self.exams[name] = Examination(name)

    def isExistedExam(self, name):
        return self.exams.get(name) != None

    def setExamScore(self, name, score):
        if self.isExistedExam(name):
            (self.exams[name]).score = score

    def toJSON(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4))

    def fromJSON(self, j):
        self.__dict__ = json.loads(j)
