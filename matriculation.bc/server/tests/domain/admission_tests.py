import unittest

import datetime

import tests.samples as samples

from matriculation.domain.admission import Order
from matriculation.domain.speciality import Speciality
from matriculation.domain.rating import Rating


class OrderTesting(unittest.TestCase):
    def test_create_empty_order(self):
        order = Order()
        self.assertEqual(order.date, datetime.date.today())
        self.assertEqual(len(order.lists), 0)
        self.assertEqual(order.text, None)

    def test_create_order_with_ratings(self):
        specs = dict()
        nSpecs = 10
        for i in range(nSpecs):
            spec = samples.createSampleSpeciality()
            specs[spec.name] = spec

        order = Order(specs)
        self.assertEqual(order.date, datetime.date.today())
        self.assertEqual(len(order.lists), nSpecs)

        with self.assertRaises(TypeError):
            order = Order("wrong")

    def test_add_speciality(self):
        order = Order()
        self.assertEqual(len(order.lists), 0)

        order.add_speciality(samples.createSampleSpeciality())
        self.assertEqual(len(order.lists), 1)

        order.add_speciality(samples.createSampleSpeciality())
        self.assertEqual(len(order.lists), 2)

        with self.assertRaises(TypeError):
            order.add_speciality("wrong")

    def test_add_existing_speciality(self):
        order = Order()
        self.assertEqual(len(order.lists), 0)

        spec = samples.createSampleSpeciality()

        order.add_speciality(spec)
        self.assertEqual(len(order.lists), 1)

        order.add_speciality(spec)
        self.assertEqual(len(order.lists), 1)


    def test_change_quota(self):
        order = Order()
        spec = samples.createSampleSpeciality()
        order.add_speciality(spec)
        self.assertEqual(order.lists[spec.name].quota, spec.quota)

        new_quota = 15
        order.set_quota(spec.name, new_quota)
        self.assertEqual(order.lists[spec.name].quota, new_quota)

        with self.assertRaises(TypeError):
            order.set_quota(spec.name, "wrong")

        wrong_quota = -42
        with self.assertRaises(ValueError):
            order.set_quota(spec.name, wrong_quota)

        wrong_spec = "No such speciality was generated in samples"
        with self.assertRaises(ValueError):
            order.set_quota(wrong_spec, new_quota)

    def test_get_final(self):
        specs = dict()
        nSpecs = 10
        for i in range(nSpecs):
            spec = samples.createSampleSpeciality()
            specs[spec.name] = spec

        order = Order(specs)
        final = order.getFinal()
        for spec in final.values():
            self.assertEqual(spec.quota, len(spec.rating.files))



suite = unittest.TestLoader().loadTestsFromTestCase(OrderTesting)
unittest.TextTestRunner(verbosity=2).run(suite)
