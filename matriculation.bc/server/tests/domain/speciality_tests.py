import unittest

import tests.samples as samples
from matriculation.domain.speciality import Speciality


class SpecialityTesting(unittest.TestCase):
    def test_create_speciality(self):
        name = 'Anykey'
        quota = 20

        spec = Speciality(name, quota)
        self.assertEqual(spec.name, name)
        self.assertEqual(spec.quota, quota)
        self.assertEqual(len(spec.rating.files), 0)

        invalid_quota = -7
        with self.assertRaises(ValueError):
            spec2 = Speciality(name, invalid_quota)

        with self.assertRaises(TypeError):
            Speciality(42, quota)

        with self.assertRaises(TypeError):
            Speciality(name, "42")

    def test_add_file(self):
        spec = samples.createSampleEmptySpeciality()
        file = samples.createSampleFile()

        spec.rating.append(file)
        self.assertEqual(len(spec.rating.files), 1)



    def test_change_quota(self):
        spec = samples.createSampleEmptySpeciality()
        spec.quota = 9
        self.assertEqual(spec.quota, 9)

        spec.quota = 44
        self.assertEqual(spec.quota, 44)

        with self.assertRaises(ValueError):
            spec.quota = -38

        with self.assertRaises(TypeError):
            spec.quota = "42"

        self.assertEqual(spec.quota, 44)


suite = unittest.TestLoader().loadTestsFromTestCase(SpecialityTesting)
unittest.TextTestRunner(verbosity=2).run(suite)
