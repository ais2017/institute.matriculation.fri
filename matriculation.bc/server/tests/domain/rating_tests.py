import unittest

import tests.samples as samples
from matriculation.domain.rating import Rating


class RatingTesting(unittest.TestCase):
    def test_createEmptyRating(self):
        rate = Rating()
        self.assertEqual(len(rate.files), 0)
        self.assertEqual(rate.final, False)

    def test_copyRating(self):
        rate = samples.createSampleRating()
        rate2 = Rating(rate)

        self.assertEqual(len(rate.files), len(rate2.files))
        self.assertEqual(rate.final, rate2.final)

        with self.assertRaises(TypeError):
            Rating(42)

    def test_addFile(self):
        rate = Rating()

        for i in range(7):
            file = samples.createSampleFile()
            rate.append(file)
            self.assertEqual(len(rate.files), i + 1)

        with self.assertRaises(TypeError):
            rate.append(42)

    def test_rmFile(self):
        rate = samples.createSampleRating()
        new_length = len(rate.files)

        for number, file in rate.files.items():
            rate.remove(number)
            new_length -= 1
            self.assertEqual(len(rate.files), new_length)

        with self.assertRaises(TypeError):
            rate.remove("42")

    def test_forbidChanges_removeChecking(self):
        rate = samples.createSampleRating()
        rate.final = True
        new_length = len(rate.files)

        for number, file in rate.files.items():
            rate.remove(number)
            self.assertEqual(len(rate.files), new_length)

        rate.final = False
        for number, file in rate.files.items():
            rate.remove(number)
            self.assertEqual(len(rate.files), new_length)

    def test_forbidChanges_addChecking(self):
        rate = samples.createSampleRating()
        new_length = len(rate.files)
        rate.final = True

        for i in range(10):
            rate.append(samples.createSampleFile())
            self.assertEqual(len(rate.files), new_length)

        rate.final = False

        for i in range(10):
            rate.append(samples.createSampleFile())
            self.assertEqual(len(rate.files), new_length)

suite = unittest.TestLoader().loadTestsFromTestCase(RatingTesting)
unittest.TextTestRunner(verbosity=2).run(suite)