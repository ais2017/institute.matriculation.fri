import unittest

import tests.samples as samples
from matriculation.domain.personellfile import Examination, PersonellFile


class ExamTesting(unittest.TestCase):
    def test_createExam(self):
        name = 'Exam1'
        exam1 = Examination(name)
        self.assertEqual(exam1.name, name)
        self.assertEqual(exam1.score, 0)

        with self.assertRaises(TypeError):
            exam2 = Examination(42)

        with self.assertRaises(ValueError):
            exam2 = Examination("")

    def test_editExam(self):

        name = 'Exam1'
        exam1 = Examination(name)
        self.assertEqual(exam1.score, 0)

        score = 30
        exam1.score = score
        self.assertEqual(exam1.score, score)

        inval_score = -26
        with self.assertRaises(ValueError):
            exam1.score = inval_score
        self.assertEqual(exam1.score, score)

        with self.assertRaises(TypeError):
            exam1.score = "score"


class PersonellTesting(unittest.TestCase):
    def test_createFile(self):
        number = 100500
        applicant = 'Sample Mr.Nobody'
        course = 'anykey'
        branch = 'sample branch'
        document = 'sample document'
        education = 'sample education place'
        original = True

        file = PersonellFile(number, applicant, course, branch, document, education, original)
        self.assertEqual(file.number, number)
        self.assertEqual(file.applicant, applicant)
        self.assertEqual(file.course, course)
        self.assertEqual(file.branch, branch)
        self.assertEqual(file.document, document)
        self.assertEqual(file.education, education)
        self.assertEqual(file.original, original)
        self.assertEqual(len(file.exams), 0)

        with self.assertRaises(TypeError):
            file = PersonellFile("42", applicant, course, branch, document, education, original)

        with self.assertRaises(TypeError):
            file = PersonellFile(number, 42, course, branch, document, education, original)

        with self.assertRaises(TypeError):
            file = PersonellFile(number, applicant, 42, branch, document, education, original)

        with self.assertRaises(TypeError):
            file = PersonellFile(number, applicant, course, 42, document, education, original)

        with self.assertRaises(TypeError):
            file = PersonellFile(number, applicant, course, branch, 42, education, original)

        with self.assertRaises(TypeError):
            file = PersonellFile(number, applicant, course, branch, document, 42, original)

        with self.assertRaises(TypeError):
            file = PersonellFile(number, applicant, course, branch, document, education, 42)

    def test_addRmOriginal(self):
        file = samples.createSampleFile()
        self.assertEqual(file.original, False)
        file.setOriginal()
        self.assertEqual(file.original, True)
        file.unSetOriginal()
        self.assertEqual(file.original, False)

    def test_addExam(self):
        file = samples.createSampleFile()
        name = 'exam1'
        name2 = 'exam2'

        file.addExam(name)
        self.assertEqual(len(file.exams), 1)

        file.addExam(name)
        self.assertEqual(len(file.exams), 1)

        file.addExam(name2)
        self.assertEqual(len(file.exams), 2)

        self.assertEqual(file.exam_score, 0)

        with self.assertRaises(TypeError):
            file.addExam(42)

        with self.assertRaises(ValueError):
            file.addExam("")

    def test_changeExam(self):
        file = samples.createSampleFile()
        name = 'exam1'
        name2 = 'exam2'

        file.addExam(name)
        self.assertEqual(file.exam_score, 0)
        self.assertEqual(len(file.exams), 1)

        score = 50

        file.setExamScore(name, score)
        self.assertEqual(file.exam_score, score)

        score2 = 60

        file.setExamScore(name2, score2)
        self.assertEqual(file.exam_score, score)  # nothing was changed

        file.addExam(name2)
        file.setExamScore(name2, score2)
        self.assertEqual(file.exam_score, score + score2)

        with self.assertRaises(TypeError):
            file.setExamScore(name, "42")

        with self.assertRaises(ValueError):
            file.setExamScore(name, -42)


suite = unittest.TestLoader().loadTestsFromTestCase(ExamTesting)
unittest.TextTestRunner(verbosity=2).run(suite)

suite = unittest.TestLoader().loadTestsFromTestCase(PersonellTesting)
unittest.TextTestRunner(verbosity=2).run(suite)
