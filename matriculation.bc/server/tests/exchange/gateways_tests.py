from exchange.gateways import DBGateway, InterfaceGateway
from tests import samples as sa

import unittest


class DBGateway_Testing(unittest.TestCase):
    # types of lists: all files included - for... not the main deal what for
    # filters: original+good exams+speciality-quota - for order ([spec[, quota, minsum?]]) what is for good exams (minsum), original is default for such filter function
    # filters: speciality - for applicant

    def setUp(self):
        self.db = DBGateway()

    def test_get_list_select_all(self):
        self.assertTrue(self.db.getListSelectWithArgs())

    def test_get_list_select_by_spec(self):
        spec = "Neuroscience"
        self.assertTrue(self.db.getListSelectWithArgs(spec))

        spec = ""
        self.assertFalse(self.db.getListSelectWithArgs(spec))

        spec = 89
        with self.assertRaises(TypeError):
            self.db.getListSelectWithArgs(spec)

    def test_get_list_select_for_order(self):
        spec = "Neuroscience"
        quota = 50
        min_exam = 50
        self.assertTrue(self.db.getListSelectWithArgs(spec, quota, min_exam))

        spec = "Neuroscience"
        quota = 0
        min_exam = 50
        self.assertFalse(self.db.getListSelectWithArgs(spec, quota, min_exam))

        spec = "Neuroscience"
        quota = "0"
        min_exam = 50
        with self.assertRaises(TypeError):
            self.db.getListSelectWithArgs(spec, quota, min_exam)

    def test_using_wrong_args(self):
        spec = "Neuroscience"
        quota = 18
        self.assertFalse(self.db.getListSelectWithArgs(spec, quota))

    def test_send_file(self):
        self.assertTrue(self.db.sendFile(sa.createSampleFile()))

        with self.assertRaises(TypeError):
            self.db.sendFile("sa.createSampleFile()")

    def test_get_file(self):
        ide = 123457
        self.assertFalse(self.db.getFile(ide))

        ide = '99999'
        with self.assertRaises(TypeError):
            self.db.getFile(ide)

    def test_update_quota(self):
        spec = "speciality"
        quota = 23
        self.assertTrue(self.db.updateQuota([spec, quota]))

        with self.assertRaises(TypeError):
            self.db.updateQuota([spec, "quota"])

        wrong_spec = 7
        with self.assertRaises(TypeError):
            self.db.updateQuota([wrong_spec, quota])

        with self.assertRaises(ValueError):
            self.db.updateQuota("none")

        with self.assertRaises(ValueError):
            self.db.updateQuota([spec])

        wrong_quota = -42
        with self.assertRaises(ValueError):
            self.db.updateQuota([spec, wrong_quota])

    def test_upload_admission(self):
        self.assertTrue(self.db.uploadAdmission(sa.createSampleOrder()))

        with self.assertRaises(TypeError):
            self.db.uploadAdmission("sa.createSampleOrder()")

    def test_get_admission(self):
        self.assertTrue(self.db.getAdmission())

    def test_send_exam(self):
        file = 666
        exam = sa.createSampleExam()
        self.assertTrue(self.db.sendExam([file, exam]))

        with self.assertRaises(TypeError):
            self.db.sendExam([file, "exam"])

        with self.assertRaises(TypeError):
            self.db.sendExam(["file", exam])

        with self.assertRaises(TypeError):
            self.db.sendExam(None)

        with self.assertRaises(ValueError):
            self.db.sendExam([file])


class InterfaceGateway_Testing(unittest.TestCase):
    def setUp(self):
        self.intrfc = InterfaceGateway()

    def test_send_file(self):
        self.assertTrue(self.intrfc.sendFile(sa.createSampleFile()))

        with self.assertRaises(TypeError):
            self.intrfc.sendFile("sa.createSampleFile()")

    def test_get_file(self):
        self.assertTrue(self.intrfc.getFile())

    def test_get_quota(self):
        self.assertTrue(self.intrfc.getQuotaPair())

    def test_get_exam(self):
        self.assertTrue(self.intrfc.getExamPair())

    def test_send_admission(self):
        self.assertTrue(self.intrfc.sendAdmission(sa.createSampleOrder()))

        with self.assertRaises(TypeError):
            self.intrfc.sendAdmission("sa.createSampleOrder()")

    def test_get_speciality(self):
        self.assertTrue(self.intrfc.getSpec())


suite = unittest.TestLoader().loadTestsFromTestCase(DBGateway_Testing)
unittest.TextTestRunner(verbosity=2).run(suite)

suite = unittest.TestLoader().loadTestsFromTestCase(InterfaceGateway_Testing)
unittest.TextTestRunner(verbosity=2).run(suite)
