from matriculation.use_cases.use_cases import GetLists
from exchange.gateways import InterfaceGateway, DBGateway

import unittest


class GetListUseCaseTesting(unittest.TestCase):
    def setUp(self):
        self.intrfc = InterfaceGateway()
        self.db = DBGateway()

    def test_get_list(self):
        case = GetLists(self.db, self.intrfc)
        self.assertTrue(case.execute())

    def test_wrong_init(self):
        with self.assertRaises(TypeError):
            case = GetLists(self.intrfc, self.db)


suite = unittest.TestLoader().loadTestsFromTestCase(GetListUseCaseTesting)
unittest.TextTestRunner(verbosity=2).run(suite)
