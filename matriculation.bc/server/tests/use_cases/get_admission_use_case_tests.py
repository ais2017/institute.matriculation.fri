from matriculation.use_cases.use_cases import GetAdmission
from exchange.gateways import InterfaceGateway, DBGateway

import unittest


class AdmissionUseCaseTesting(unittest.TestCase):
    def setUp(self):
        self.intrfc = InterfaceGateway()
        self.db = DBGateway()

    def test_get_admission(self):
        case = GetAdmission(self.db, self.intrfc)
        self.assertTrue(case.execute())

    def test_wrong_init(self):
        with self.assertRaises(TypeError):
            case = GetAdmission(self.intrfc, self.db)


suite = unittest.TestLoader().loadTestsFromTestCase(AdmissionUseCaseTesting)
unittest.TextTestRunner(verbosity=2).run(suite)
