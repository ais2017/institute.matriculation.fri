from matriculation.use_cases.use_cases import Registration
from exchange.gateways import InterfaceGateway, DBGateway

import unittest


class RegisterTesting(unittest.TestCase):
    def setUp(self):
        self.intrfc = InterfaceGateway()
        self.db = DBGateway()

    def test_add(self):
        case = Registration(self.db, self.intrfc)
        self.assertTrue(case.execute("reg"))

    def test_exam_add(self):
        case = Registration(self.db, self.intrfc)
        self.assertTrue(case.execute("exam"))

    def test_wrong_exec(self):
        case = Registration(self.db, self.intrfc)
        self.assertFalse(case.execute("wrong"))

    def test_wrong_init(self):
        with self.assertRaises(TypeError):
            case = Registration(self.intrfc, self.db)


suite = unittest.TestLoader().loadTestsFromTestCase(RegisterTesting)
unittest.TextTestRunner(verbosity=2).run(suite)
