from matriculation.use_cases.use_cases import EditAdmission
from exchange.gateways import InterfaceGateway, DBGateway

import unittest


class ChangeQuotaTesting(unittest.TestCase):
    def setUp(self):
        self.intrfc = InterfaceGateway()
        self.db = DBGateway()

    def test_get_quota(self):
        case = EditAdmission(self.db, self.intrfc)
        self.assertTrue(case.execute())

    def test_wrong_init(self):
        with self.assertRaises(TypeError):
            case = EditAdmission(self.intrfc, self.db)


suite = unittest.TestLoader().loadTestsFromTestCase(ChangeQuotaTesting)
unittest.TextTestRunner(verbosity=2).run(suite)
