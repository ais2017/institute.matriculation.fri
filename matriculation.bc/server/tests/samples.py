import random

from matriculation.domain.personellfile import PersonellFile, Examination
from matriculation.domain.rating import Rating
from matriculation.domain.speciality import Speciality
from matriculation.domain.admission import Order


def createSampleFile(ide=None, spec=None):
    if ide == None:
        number = random.randint(10000, 100000)
    else:
        number = ide

    applicant = 'Sample Mr.Nobody'
    if spec == None:
        course = 'Anykey'
    else:
        course = spec
    branch = 'sample Connemara'
    document = 'sample 141090'
    education = 'sample education place'
    original = False

    return PersonellFile(number, applicant, course, branch, document, education, original)


def createSampleRating(spec=None, quota=None, minsum=None):
    if quota == None:
        nFiles = random.randint(10, 100)
    else:
        nFiles = random.randint(0, quota)

    rating = Rating()

    for i in range(nFiles):
        rating.append(createSampleFile(None, spec))

    return rating


def createSampleEmptySpeciality():
    quota = 20
    name = 'Speciality' + str(random.randint(100, 1000))

    return Speciality(name, quota)


def createSampleSpeciality():
    spec = createSampleEmptySpeciality()

    nFiles = 40

    for i in range(nFiles - 1):
        spec.rating.append(createSampleFile())

    return spec


def createSampleExam():
    name = 'Exam code' + str(random.randint(100, 1000))
    exam = Examination(name)
    exam.score = random.randint(0, 100)

    return exam


def createSampleOrder():
    specs = dict()
    nSpecs = 10
    for i in range(nSpecs):
        spec = createSampleSpeciality()
        specs[spec.name] = spec

    return Order(specs)
