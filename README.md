# Описание организации
Организация осуществляет подготовку бакалавров, магистров и аспирантов по ряду различных направлений. Имеется несколько филиалов, расположенных в разных городах. Помимо образовательной деятельности, также осуществляется и научная.
# Описание области автоматизации
Система должна помогать сотрудникам приемной комиссии работать с абитуриентами, вычислять студентов, прошедших конкурс и подготавливать приказы о зачислении.